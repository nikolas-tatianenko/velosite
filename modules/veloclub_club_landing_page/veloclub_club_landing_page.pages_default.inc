<?php
/**
 * @file
 * veloclub_club_landing_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function veloclub_club_landing_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'club';
  $page->task = 'page';
  $page->admin_title = 'Club';
  $page->admin_description = '';
  $page->path = 'club';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Club',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_club_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'club';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'landing-page-wrapper club-landing-page-wrapper',
    'css_id' => 'landing-page',
    'css' => 'landing-page',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'omega_12_twocol_3_9';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7ededd07-b256-4da1-b755-1c11542da0ea';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0e72ca9c-fbd0-405b-b0e7-09f330f112f9';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-club-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0e72ca9c-fbd0-405b-b0e7-09f330f112f9';
    $display->content['new-0e72ca9c-fbd0-405b-b0e7-09f330f112f9'] = $pane;
    $display->panels['left'][0] = 'new-0e72ca9c-fbd0-405b-b0e7-09f330f112f9';
    $pane = new stdClass();
    $pane->pid = 'new-e85a9fdc-b252-4b0b-9958-a72078dacc0a';
    $pane->panel = 'right';
    $pane->type = 'views_panes';
    $pane->subtype = 'club_main_page-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e85a9fdc-b252-4b0b-9958-a72078dacc0a';
    $display->content['new-e85a9fdc-b252-4b0b-9958-a72078dacc0a'] = $pane;
    $display->panels['right'][0] = 'new-e85a9fdc-b252-4b0b-9958-a72078dacc0a';
    $pane = new stdClass();
    $pane->pid = 'new-175e6903-2dbc-4841-b631-b379b25f061d';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_5',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'big-news-block-wrapper',
      'css_class' => 'big-news-block-wrapper',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '175e6903-2dbc-4841-b631-b379b25f061d';
    $display->content['new-175e6903-2dbc-4841-b631-b379b25f061d'] = $pane;
    $display->panels['right'][1] = 'new-175e6903-2dbc-4841-b631-b379b25f061d';
    $pane = new stdClass();
    $pane->pid = 'new-14bef074-da75-43fc-b80a-a9156c11876c';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'slideshow';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '10',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '14bef074-da75-43fc-b80a-a9156c11876c';
    $display->content['new-14bef074-da75-43fc-b80a-a9156c11876c'] = $pane;
    $display->panels['right'][2] = 'new-14bef074-da75-43fc-b80a-a9156c11876c';
    $pane = new stdClass();
    $pane->pid = 'new-0083c91b-fd92-46da-bafb-fb014e4cfffe';
    $pane->panel = 'right';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '0083c91b-fd92-46da-bafb-fb014e4cfffe';
    $display->content['new-0083c91b-fd92-46da-bafb-fb014e4cfffe'] = $pane;
    $display->panels['right'][3] = 'new-0083c91b-fd92-46da-bafb-fb014e4cfffe';
    $pane = new stdClass();
    $pane->pid = 'new-8444451b-9819-4dc3-926b-fc038495b3b2';
    $pane->panel = 'right';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '105',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'teaser',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '8444451b-9819-4dc3-926b-fc038495b3b2';
    $display->content['new-8444451b-9819-4dc3-926b-fc038495b3b2'] = $pane;
    $display->panels['right'][4] = 'new-8444451b-9819-4dc3-926b-fc038495b3b2';
    $pane = new stdClass();
    $pane->pid = 'new-f4664d68-f9af-423e-a369-7df2ae40448d';
    $pane->panel = 'right';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '108',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'teaser',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'f4664d68-f9af-423e-a369-7df2ae40448d';
    $display->content['new-f4664d68-f9af-423e-a369-7df2ae40448d'] = $pane;
    $display->panels['right'][5] = 'new-f4664d68-f9af-423e-a369-7df2ae40448d';
    $pane = new stdClass();
    $pane->pid = 'new-ae71c88b-5157-4a1d-8e8e-9cf85994bfc7';
    $pane->panel = 'right';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '110',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'teaser',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'ae71c88b-5157-4a1d-8e8e-9cf85994bfc7';
    $display->content['new-ae71c88b-5157-4a1d-8e8e-9cf85994bfc7'] = $pane;
    $display->panels['right'][6] = 'new-ae71c88b-5157-4a1d-8e8e-9cf85994bfc7';
    $pane = new stdClass();
    $pane->pid = 'new-c21b4aeb-b70e-4659-8fbc-4cd170654e10';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '7',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'main-news-block',
      'css_class' => 'main-news-block',
    );
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'c21b4aeb-b70e-4659-8fbc-4cd170654e10';
    $display->content['new-c21b4aeb-b70e-4659-8fbc-4cd170654e10'] = $pane;
    $display->panels['right'][7] = 'new-c21b4aeb-b70e-4659-8fbc-4cd170654e10';
    $pane = new stdClass();
    $pane->pid = 'new-3bbf2418-d7f3-43d9-93d6-508ac3a1ff76';
    $pane->panel = 'right';
    $pane->type = 'panels_mini';
    $pane->subtype = 'right_panel';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $pane->uuid = '3bbf2418-d7f3-43d9-93d6-508ac3a1ff76';
    $display->content['new-3bbf2418-d7f3-43d9-93d6-508ac3a1ff76'] = $pane;
    $display->panels['right'][8] = 'new-3bbf2418-d7f3-43d9-93d6-508ac3a1ff76';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['club'] = $page;

  return $pages;

}
