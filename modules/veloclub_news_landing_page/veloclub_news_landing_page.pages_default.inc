<?php
/**
 * @file
 * veloclub_news_landing_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function veloclub_news_landing_page_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'News page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'content-type-panel',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'news' => 'news',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'omega_12_twocol_3_9';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '7ed5c124-d954-4213-8860-4ab70738bf02';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e2a0379a-8388-4391-b06c-f0a36d206bc0';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-news-category';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e2a0379a-8388-4391-b06c-f0a36d206bc0';
    $display->content['new-e2a0379a-8388-4391-b06c-f0a36d206bc0'] = $pane;
    $display->panels['left'][0] = 'new-e2a0379a-8388-4391-b06c-f0a36d206bc0';
    $pane = new stdClass();
    $pane->pid = 'new-e6cb155d-8c1f-4e2c-811a-dcb9fcbe185e';
    $pane->panel = 'right';
    $pane->type = 'panels_mini';
    $pane->subtype = 'right_panel';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e6cb155d-8c1f-4e2c-811a-dcb9fcbe185e';
    $display->content['new-e6cb155d-8c1f-4e2c-811a-dcb9fcbe185e'] = $pane;
    $display->panels['right'][0] = 'new-e6cb155d-8c1f-4e2c-811a-dcb9fcbe185e';
    $pane = new stdClass();
    $pane->pid = 'new-9ab37033-4113-4c73-b3d5-58f224e43715';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'filtered_html',
      'substitute' => TRUE,
      'name' => 'share_buttons',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9ab37033-4113-4c73-b3d5-58f224e43715';
    $display->content['new-9ab37033-4113-4c73-b3d5-58f224e43715'] = $pane;
    $display->panels['right'][1] = 'new-9ab37033-4113-4c73-b3d5-58f224e43715';
    $pane = new stdClass();
    $pane->pid = 'new-48fcd167-b488-4090-b305-d12bfe4d1df6';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '48fcd167-b488-4090-b305-d12bfe4d1df6';
    $display->content['new-48fcd167-b488-4090-b305-d12bfe4d1df6'] = $pane;
    $display->panels['right'][2] = 'new-48fcd167-b488-4090-b305-d12bfe4d1df6';
    $pane = new stdClass();
    $pane->pid = 'new-84fb8f7b-f57e-4d5c-9e03-f31e80f11f93';
    $pane->panel = 'right';
    $pane->type = 'veloclub_news_title';
    $pane->subtype = 'veloclub_news_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'text' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '84fb8f7b-f57e-4d5c-9e03-f31e80f11f93';
    $display->content['new-84fb8f7b-f57e-4d5c-9e03-f31e80f11f93'] = $pane;
    $display->panels['right'][3] = 'new-84fb8f7b-f57e-4d5c-9e03-f31e80f11f93';
    $pane = new stdClass();
    $pane->pid = 'new-459870f9-d010-40fc-8da1-d0c71445d494';
    $pane->panel = 'right';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => '',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '459870f9-d010-40fc-8da1-d0c71445d494';
    $display->content['new-459870f9-d010-40fc-8da1-d0c71445d494'] = $pane;
    $display->panels['right'][4] = 'new-459870f9-d010-40fc-8da1-d0c71445d494';
    $pane = new stdClass();
    $pane->pid = 'new-6f7aea0f-ad12-4551-9c97-ec767ff5f90c';
    $pane->panel = 'right';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '6f7aea0f-ad12-4551-9c97-ec767ff5f90c';
    $display->content['new-6f7aea0f-ad12-4551-9c97-ec767ff5f90c'] = $pane;
    $display->panels['right'][5] = 'new-6f7aea0f-ad12-4551-9c97-ec767ff5f90c';
    $pane = new stdClass();
    $pane->pid = 'new-af6a91ff-1451-40cf-aaaf-34b01022e4ea';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'af6a91ff-1451-40cf-aaaf-34b01022e4ea';
    $display->content['new-af6a91ff-1451-40cf-aaaf-34b01022e4ea'] = $pane;
    $display->panels['right'][6] = 'new-af6a91ff-1451-40cf-aaaf-34b01022e4ea';
    $pane = new stdClass();
    $pane->pid = 'new-ca85f08c-b342-4a06-8193-0bc3332af684';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '7',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_8',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'similar-news-wrapper',
      'css_class' => 'similar-news-wrapper',
    );
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'ca85f08c-b342-4a06-8193-0bc3332af684';
    $display->content['new-ca85f08c-b342-4a06-8193-0bc3332af684'] = $pane;
    $display->panels['right'][7] = 'new-ca85f08c-b342-4a06-8193-0bc3332af684';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function veloclub_news_landing_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'news';
  $page->task = 'page';
  $page->admin_title = 'News';
  $page->admin_description = '';
  $page->path = 'news';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'News',
    'name' => 'main-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_news_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'news';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'landing-page-wrapper',
    'css_id' => 'landing-page',
    'css' => 'landing-page',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'omega_12_twocol_3_9';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '48b7245b-cd6a-4e37-9251-7a8908722f18';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-49b93a5d-aa52-4598-a5b2-5bc3a8d49716';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-news-category';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '49b93a5d-aa52-4598-a5b2-5bc3a8d49716';
    $display->content['new-49b93a5d-aa52-4598-a5b2-5bc3a8d49716'] = $pane;
    $display->panels['left'][0] = 'new-49b93a5d-aa52-4598-a5b2-5bc3a8d49716';
    $pane = new stdClass();
    $pane->pid = 'new-2765c5e2-cc15-4f06-b1c1-3c78910af4eb';
    $pane->panel = 'right';
    $pane->type = 'panels_mini';
    $pane->subtype = 'news_6_blocks';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2765c5e2-cc15-4f06-b1c1-3c78910af4eb';
    $display->content['new-2765c5e2-cc15-4f06-b1c1-3c78910af4eb'] = $pane;
    $display->panels['right'][0] = 'new-2765c5e2-cc15-4f06-b1c1-3c78910af4eb';
    $pane = new stdClass();
    $pane->pid = 'new-ef910898-18df-410a-8539-90fd06bdcb2b';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_5',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'big-news-block-wrapper',
      'css_class' => 'big-news-block-wrapper',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ef910898-18df-410a-8539-90fd06bdcb2b';
    $display->content['new-ef910898-18df-410a-8539-90fd06bdcb2b'] = $pane;
    $display->panels['right'][1] = 'new-ef910898-18df-410a-8539-90fd06bdcb2b';
    $pane = new stdClass();
    $pane->pid = 'new-cdc72dfd-2e09-4590-aa20-118a2bae5cc8';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'slideshow';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '10',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'cdc72dfd-2e09-4590-aa20-118a2bae5cc8';
    $display->content['new-cdc72dfd-2e09-4590-aa20-118a2bae5cc8'] = $pane;
    $display->panels['right'][2] = 'new-cdc72dfd-2e09-4590-aa20-118a2bae5cc8';
    $pane = new stdClass();
    $pane->pid = 'new-14ba8dcc-3cd1-4337-9bcc-50cfa1ea5e60';
    $pane->panel = 'right';
    $pane->type = 'page_title';
    $pane->subtype = 'page_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'markup' => 'h1',
      'class' => '',
      'id' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '14ba8dcc-3cd1-4337-9bcc-50cfa1ea5e60';
    $display->content['new-14ba8dcc-3cd1-4337-9bcc-50cfa1ea5e60'] = $pane;
    $display->panels['right'][3] = 'new-14ba8dcc-3cd1-4337-9bcc-50cfa1ea5e60';
    $pane = new stdClass();
    $pane->pid = 'new-922a5968-d5d3-41d3-94f8-acf63d35320c';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '7',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'main-news-block',
      'css_class' => 'main-news-block',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '922a5968-d5d3-41d3-94f8-acf63d35320c';
    $display->content['new-922a5968-d5d3-41d3-94f8-acf63d35320c'] = $pane;
    $display->panels['right'][4] = 'new-922a5968-d5d3-41d3-94f8-acf63d35320c';
    $pane = new stdClass();
    $pane->pid = 'new-d67b9a58-6376-4f9f-ba26-3df2183db522';
    $pane->panel = 'right';
    $pane->type = 'panels_mini';
    $pane->subtype = 'right_panel';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'd67b9a58-6376-4f9f-ba26-3df2183db522';
    $display->content['new-d67b9a58-6376-4f9f-ba26-3df2183db522'] = $pane;
    $display->panels['right'][5] = 'new-d67b9a58-6376-4f9f-ba26-3df2183db522';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['news'] = $page;

  return $pages;

}
