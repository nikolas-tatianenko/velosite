<p><span class='company-name'><?php print t(strtoupper($company)); ?></span></p>
<p><span class='address'><?php print t($address_part1); ?></span></p>
<p><span class='address'><?php print t($address_part2); ?></span></p>
<p><span
    class='email'><?php print t('E-mail:'); ?></span><span><?php print $email; ?></span>
</p>
<table border='0'>
  <tr>
    <td><?php print t('Phone/fax:'); ?></td>
    <td><?php print $phone_1; ?></td>
    <td></td>
  </tr>
  <tr>
    <td><?php print t('Phones:'); ?></td>
    <td><?php print $phone_2; ?></td>
    <td><?php print $phone_4; ?></td>
  </tr>
  <tr>
    <td></td>
    <td><?php print $phone_3; ?></td>
    <td><?php print $phone_5; ?></td>
  </tr>
</table>