<div class='block-content-image <?php print $additional_classes;?>'>
  <?php print $content; ?>
</div>
<div class='block-content-footer'>
  <?php if ($link_text) {
    print l($link_text, $link_url);
  } ?>
</div>
