<?php
// This file must be at mymodule/views directory.
 
/**
 * @file
 * Views definitions for mymodule module.
 */
 
/**
 * Implements hook_views_data().
 */
function veloclub_main_views_data() {
  $data = array();
  $data['node']['field_collection_list'] = array(
    'title' => t('Velocity field collection list'),
    'help' => t('Description of my handler.'),
    'field' => array(
      'handler' => 'veloclub_main_handler_field_collection_list',
    ),
  );
  return $data;
}


/**
* Implements hook_views_query_alter().
*/
function veloclub_main_views_query_alter(&$views) {
  switch ($views->name) {
  case 'cyclist_materials':
  
	$node = node_load(arg(1));
	if($node){
	$translations = translation_node_get_translations($node->tnid);
  
	$translated_node_ids = array();
  
	  foreach($translations as $translation) {
		$translated_node_ids[] = $translation->nid;
	  }

    switch ($views->current_display) {
		case 'block':
		case 'block_1':
		case 'block_2':
		case 'block_3':
		case 'block_4':
		case 'block_5':
		case 'block_6':
			$views->args = $translated_node_ids;
		break;
	}
	}
  break;
	  case 'news':
		  switch ($views->current_display) {
			case 'block_8':
				$views->query->where[1]['conditions'][] = array(
				  'field' => 'node.nid',
				  'value' => arg(1),
				  'operator' => ' !=',
				);
				break;
		  }
	}
}

/**
* Implements hook_views_pre_render().
*/
function veloclub_main_views_pre_render(&$views) {
  global $language;
  switch ($views->name) {
    case 'slideshow':
		switch ($views->current_display) {
		   case 'block':
		   break;
		}
	break;
    case 'team':
      switch ($views->current_display) {
        case 'block':
          $link = 'node/';
          if ($language->language == 'ru') {
            $nid = 296;
          }
          else {
            $nid = 369;
          }
          $link .= $nid;
          $node = node_load($nid);

          $views->footer['area']->options['content'] = t('Coach') . ':' . l($node->title, $link);
          break;
        case 'block_1':
          $link = 'node/';
          if ($language->language == 'ru') {
            $nid = 359;
          }
          else {
            $nid = 368;
          }
          $link .= $nid;
          $node = node_load($nid);
          $views->footer['area']->options['content'] = t('Coach') . ':' . l($node->title, $link);
          break;
      }
      break;
    case 'races':
	$class_collection = array('open-results', 'closed');
      switch ($views->current_display) {
        case 'page':
		case 'page_1':
		foreach ($views->result as &$row) {
			if (count($row->field_field_results_collection)) {
				$class_collection[] = 'has-results';
			} else {
				$class_collection[] = 'no-results';
			}
			// Month date.
			
			foreach ($row->field_field_date as &$field_date) {
				$field_date['rendered']['#markup']="<div class='".implode(' ', $class_collection)."'><div class='trigger'>+</div><div class='date'>";
				
				if (isset($field_date['raw']['value2']) && (date('d', $field_date['raw']['value']) != date('d', $field_date['raw']['value2']))) {
					$field_date['rendered']['#markup'] .= date('d', $field_date['raw']['value']) . '-' . date('d', $field_date['raw']['value2']) . ' ' . t(date('F', $field_date['raw']['value']));
				} else {
					$field_date['rendered']['#markup'] .= date('d', $field_date['raw']['value']) . ' ' . t(date('F', $field_date['raw']['value']));
				}
				
				$field_date['rendered']['#markup'].="</div></div>";
			}
			}			
		break;
        case 'block_2':
		foreach ($views->result as &$row) {
		foreach ($row->field_field_date as &$field_date) {
				$field_date['rendered']['#markup']="<div class='date'>";
				
				if (isset($field_date['raw']['value2']) && (date('d', $field_date['raw']['value']) != date('d', $field_date['raw']['value2']))) {
				
					if ((int) date('m', $field_date['raw']['value']) == (int) date('m', $field_date['raw']['value2'])) {
						  $field_date['rendered']['#markup'] .=  date('d', $field_date['raw']['value']) . '-' . date('d F', $field_date['raw']['value2']);
						}
						else {
						  $field_date['rendered']['#markup'] .=  date('d.m', $field_date['raw']['value']) . '-' . date('d.m', $field_date['raw']['value2']);
						}
				} else {
					$field_date['rendered']['#markup'] .= date('d', $field_date['raw']['value']) . ' ' . t(date('F', $field_date['raw']['value']));
				}
				
				
				
				
				$field_date['rendered']['#markup'].="</div>";
			}
		}
		break;
        case 'block_3':
        case 'block_5':
          if ($views->current_display == 'block_3') {
            $date = date('F Y', strtotime('first day of this month'));
          }
          else {
            $date = date('F Y', strtotime('first day of next month'));
          }

          list($month_name, $year) = explode(' ', $date);
          $views->header['area_1']->options['content'] = format_string('@month_name @year', array(
            '@month_name' => t($month_name),
            '@year' => $year
          ));
          $views->header['area_1']->options['format'] = 'plain_text';
		  
		  foreach ($views->result as &$row) {
			if (count($row->field_field_results_collection)) {
				$class_collection[] = 'has-results';
			} else {
				$class_collection[] = 'no-results';
			}
			// Month date.
			
			foreach ($row->field_field_date as &$field_date) {
				$field_date['rendered']['#markup']="<div class='".implode(' ', $class_collection)."'><div class='trigger'>+</div><div class='date'>";
				
				if (isset($field_date['raw']['value2']) && (date('d', $field_date['raw']['value']) != date('d', $field_date['raw']['value2']))) {
					$field_date['rendered']['#markup'] .= date('d', $field_date['raw']['value']) . '-' . date('d', $field_date['raw']['value2']) . ' ' . t(date('F', $field_date['raw']['value']));
				} else {
					$field_date['rendered']['#markup'] .= date('d', $field_date['raw']['value']) . ' ' . t(date('F', $field_date['raw']['value']));
				}
				
				$field_date['rendered']['#markup'].="</div></div>";
			}
			}
          break;
        case 'block_6':
		$mounthes = array(
			  1 => t('Jan'),
			  2 => t('Feb'),
			  3 => t('Mar'),
			  4 => t('Apr'),
			  5 => t('May'),
			  6 => t('Jun'),
			  7 => t('Jul'),
			  8 => t('Aug'),
			  9 => t('Sep'),
			  10 => t('Oct'),
			  11 => t('Nov'),
			  12 => t('Des'),
		);
        foreach ($views->result as &$r) {
			// Month name.
			foreach ($r->field_field_date_1 as &$field_date_1) {
				$field_date_1['rendered']['#markup']= '<div class="date">'.$mounthes[date('n', $field_date_1[0]["raw"]["value"])].'</div>';
			}
			// Month date.
			foreach ($r->field_field_date as &$field_date) {
				if (isset($field_date['raw']['value2']) && (date('d', $field_date['raw']['value']) != date('d', $field_date['raw']['value2']))) {
					$field_date['rendered']['#markup'] = date('d', $field_date['raw']['value']) . '-' . date('d', $field_date['raw']['value2']);
				} else {
					$field_date['rendered']['#markup'] = date('d', $field_date['raw']['value']);
				}
			}
			
            foreach ($r->field_field_venue as &$venue_val) {
              $term = $venue_val['raw']['taxonomy_term'];
              $translated_term = i18n_taxonomy_localize_terms($term);
              $venue_val['raw']['taxonomy_term'] = $translated_term;
              $venue_val['rendered']['#markup'] = $translated_term->name;
            }
			
            foreach ($r->field_field_discipline as &$venue_val) {
              $term = $venue_val['raw']['taxonomy_term'];
              $translated_term = i18n_taxonomy_localize_terms($term);
              $venue_val['raw']['taxonomy_term'] = $translated_term;
              $venue_val['rendered']['#markup'] = $translated_term->name;
            }
          }
          break;
      }
      break;
    case 'cyclist_materials':
	foreach ($views->result as &$row) {
	  if(isset($row->field_field_date)){
			foreach ($row->field_field_date as &$field_date) {
				if (isset($row->field_field_date[0]['raw']['value2'])) {
					$field_date['rendered']['#markup'] = date('d', $row->field_field_date[0]['raw']['value']) . '-' . date('d.m.Y', $row->field_field_date[0]['raw']['value2']);
				}else {
					$field_date['rendered']['#markup'] = date('d.m.Y', $row->field_field_date[0]['raw']['value']);
				}
				}
				}/**
				if(isset($row->field_field_position)){
				dsm($row);
				foreach ($row->field_field_position as &$field_value) {
				$field_value['rendered']['#markup'] ='<span>— </span><span>'.$field_value['rendered']['#markup'].'</span><span>'.t('-th place').'</span>';
				}
				}*/
			}
      switch ($views->current_display) {
	  
        case 'block_3':
		    
		break;
        case 'block_4':
          $result = array();
          foreach ($views->result as $res) {

            if (!isset($result[$res->nid]) && count($result) == 0) {
              $result[$res->nid] = $res;
            }
            elseif (isset($result[$res->nid])) {
              $result[$res->nid]->field_field_type_uci_race[] = $res->field_field_type_uci_race[0];
              $result[$res->nid]->field_field_position[] = $res->field_field_position[0];
              $result[$res->nid]->_field_data = array();
            }
          }
          $views->result = array_values($result);
          break;
      }
      break;

  }
}

/*
function veloclub_main_views_pre_view(&$view, &$display_id, &$args) {

  switch($view->name){
    case 'rating_uci':
	//dsm($view);
	//dsm($view->display[$display_id]);
	//var_dump($view);
	//exit();
      break;
    case 'news':
   //  dsm($view->display[$display_id]);
   //   dsm($view->display[$display_id]->handler);
 //     dsm($view->display[$display_id]->handler->handlers);
  //    dsm($view->display[$display_id]->handler->handlers['footer']);
 //     dsm($view->display[$display_id]->handler->handlers['footer']['area_text_custom']->options['content']);
      break;
    default:
      break;
  }
}*/