﻿<?php
// This file must be at mymodule/views directory.
 
/**
 * @file
 * Definition of mymodule_handler_handlername.
 */
 
/**
 * Description of what my handler does.
 */
class veloclub_main_handler_field_collection_list extends views_handler_field {
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
    $this->additional_fields['field_something'] = array(
      'table' => 'field_data_field_something',
      'field' => 'field_something_value',
    );
  }
 
  /**
   * Loads additional fields.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }
 
  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();
 
    $options['option_a'] = array('default' => '');
    $options['option_b'] = array('default' => '');
 
    return $options;
  }
 
  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
 
    $form['option_a'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $this->options['option_a'],
      '#description' => t('Some description.'),
      '#weight' => -10,
    );
 
    $form['option_b'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $this->options['option_b'],
      '#description' => t('Some description.'),
      '#weight' => -9,
    );
  }
 
  /**
   * Renders the field handler.
   */
  function render($values) {
  if(isset($values->field_field_position)) {
    foreach($values->field_field_position as $delta =>$val) {
	
	$values->field_results_collection[]=format_string('<div>
	<div class="views-field views-field-field-type-uci-race"><div class="field-content">@field_type_uci_race</div></div>
	<div class="views-field views-field-field-position"><div class="field-content"><span> </span><span>@position_number</span><span>@position_text</span>
	</div></div></div>', 
		array(
			'@field_type_uci_race' => $values->field_field_type_uci_race[$delta]['rendered']['#markup'],
			'@position_number'    => $val['rendered']['#markup'],
			'@position_text'      => t('-th place'),
			)
		);
		}
		return theme('item_list',array('items' => $values->field_results_collection));
	} 
  }
}