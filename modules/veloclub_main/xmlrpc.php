<?php

/**
 * @file
 * The XML Node export module.
 *
 * Allows users to export nodes and then import them into another Drupal installation.
 */
define('xmlrpc_synch_file_FILES_DIR_SUBSTITUTION', '#FILES_DIRECTORY_PATH#');

/**
 * Implements hook_xmlrpc().
 */
function xmlrpc_synch_xmlrpc() {
  return array(
    'xmlrpc_synch.get_test' => 'xmlrpc_synch_get_test',
    'xmlrpc_synch.get_data_count' => 'xmlrpc_synch_get_data_count',
    'xmlrpc_synch.get_all_data' => 'xmlrpc_synch_get_all_data',
    'xmlrpc_synch.get_last_data' => 'xmlrpc_synch_get_last_data_server',
    'xmlrpc_synch.get_latest_data' => 'xmlrpc_synch_get_latest_server',
    'xmlrpc_synch.get_node_by_uuid' => 'xmlrpc_synch_get_node_by_uuid',
    'xmlrpc_synch.get_all_views' => 'xmlrpc_synch_get_all_views',
    'xmlrpc_synch.get_last_week_data' => 'xmlrpc_synch_get_last_week_data',
    'xmlrpc_synch.get_last_three_days_data' => 'xmlrpc_synch_get_last_three_days_data',
    'xmlrpc_synch.get_data' => 'xmlrpc_synch_get_data',
    'xmlrpc_synch.check_types' => 'xmlrpc_synch_check_types',
    'xmlrpc_synch.check_taxonomy' => 'xmlrpc_synch_check_taxonomy',
    'xmlrpc_synch.check_permission' => 'xmlrpc_synch_check_permission',
  );
}

function xmlrpc_synch_get_list_of_views() {
  $exportables = array();
  $exportables2 = array();
  foreach (module_implements('views_exportables') as $module) {
    $function = $module . '_views_exportables';
    $exportables[$module] = $function('list', array());
    asort($exportables[$module]);
  }
  foreach ($exportables['views'] as $key => $views) {
    $exportables2[$key] = $views['name'];
  }
  return $exportables2;
}

function xmlrpc_synch_get_all_views_data($views) {
  $views = array_filter($views);
  asort($views);
  if ($views) {
    //$code .= module_invoke('views', 'views_exportables', 'export', $views, 'testmodule') . "\n\n";
  }
  $code = xmlrpc_synch_views_exportables($views);
  $lines = substr_count($code, "\n");
  return $code;
}

function xmlrpc_synch_views_exportables($views = NULL) {
  $all_views = views_get_all_views();
  $return = array();
  foreach ($views as $view => $truth) {
    $return[] = array(
      'name' => $all_views[$view]->name,
      'view' => $all_views[$view]->export('  '),
    );
  }

  return $return;
}

/**
 * Implementation of hook_menu().
 */
function xmlrpc_synch_cron() {


  $cron_enable = variable_get('xmlrpc_synch_cron_enable', FALSE);

  if ($cron_enable) {


    $last_data_check = variable_get('xmlrpc_synch_last_data_check', 0);

    $count = xmlrpc_synch_get_count($last_data_check);

//	drupal_set_message('last data check ->'.print_r($last_data_check,true));
//	drupal_set_message('Count ->'.$count);

    $url = variable_get('xmlrpc_synch_server_url', 'http://lukukla.ru/xmlrpc.php');


    if ($count >= 10) {
      $count = 10;
    }
    else {
      $count = 1;
    }


    $data = xmlrpc($url, 'xmlrpc_synch.get_data', $last_data_check, 0, $count);
    //$data['data'];

    variable_set('xmlrpc_synch_last_data_check', $data['changed']);
//	drupal_set_message('last data check ->'. $data['changed']);
//	drupal_set_message("<pre>".print_r($data,true)."</pre>");
    $res = xmlrpc_synch_import($data['data']);


    //sleep(20);
  }
  else {

  }
}

function xmlrpc_synch_menu() {
  $items = array(); //

  $items['xml_server'] = array(
    'title' => 'xml server',
    'page callback' => 'xmlrpc_synch_server',
    'access callback' => FALSE, //
    'type' => MENU_NORMAL_ITEM,
    'file' => 'xmlrpc_synch.server.inc'
  );


  $items['xml_client'] = array(
    'title' => 'xml server',
    'page callback' => 'xmlrpc_synch_client',
    'access callback' => FALSE, //
    'type' => MENU_NORMAL_ITEM,
    'file' => 'xmlrpc_synch.client.inc'
  );

  $items['admin/settings/xmlrpc_synch/sync'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_syncronise_node'),
    'title' => 'Синхронизация материалов',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );


  $items['admin/settings/xmlrpc_synch/no_image_nodes'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_no_image_nodes'),
    'title' => 'Перепроверка изщображений',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );


  $items['xml_views_export'] = array(
    'title' => 'xml server',
    'page callback' => 'xmlrpc_synch_views_export',
    'access callback' => FALSE, //
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/xmlrpc_synch/xml_client'] = array(
    'title' => 'xml client',
    'page callback' => 'xmlrpc_synch_client',
    'access arguments' => array('administer site configuration'),
    'file' => 'xmlrpc_synch.client.inc',
    'type' => MENU_NORMAL_ITEM
  );


  $items['admin/settings/xmlrpc_synch'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_settings'),
    'title' => 'Синхронизация',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );

  $items['admin/settings/images_update'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_images_update_page'),
    'title' => 'Обновление , восстановление картинок',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );

  $items['admin/settings/xmlrpc_synch/fast_node_update'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_fast_node_update'),
    'title' => 'Быстрая работа с нодами',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );

  $items['admin/settings/xmlrpc_synch/cron_enable'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_cron_enable_form'),
    'title' => 'cron_enable',
    'description' => 'Configure the settings for Node export.',
    //'file' => 'xmlrpc_synch.pages.inc',
  );
  $items['admin/settings/xmlrpc_synch/views_update'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_views_update_form'),
    'title' => 'Обновить views',
    'description' => 'Configure the settings for Node export.',
    'file' => 'xmlrpc_synch.pages.inc',
  );

  $items['admin/settings/xmlrpc_synch/product_settings'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_product_settings_form'),
    'title' => 'product settings',
    'description' => 'Product settings',
    'file' => 'xmlrpc_synch.pages.inc',
  );

  $items['admin/settings/xmlrpc_synch/types_settings'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_types_settings'),
    'title' => 'types_settings',
    'description' => 'Configure the settings for Node export.',
    //'file' => 'xmlrpc_synch.pages.inc',
  );
  /*
    $items['node/%node/xmlrpc_synch'] = array(
    'access callback'  => 'xmlrpc_synch_node_synch',
    'access arguments' => array('administer site configuration'),
    'page callback'    => 'xmlrpc_synch_node_synch',
    'page arguments'   => array(1),
    'title'            => 'Синхнонизировать',
    'weight'           => 5,
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'xmlrpc_synch.pages.inc',
    );
   */
  /*
    $selected_formats = variable_get('xmlrpc_synch_format', array('node_code'));

    if (count(array_filter($selected_formats)) > 1) {
    $format_handlers = xmlrpc_synch_format_handlers();
    foreach ($format_handlers as $format_handler => $format) {
    if (in_array($format_handler, $selected_formats)) {
    $items['node/%node/xmlrpc_synch/'. $format_handler] = array(
    'access callback' => 'xmlrpc_synch_access_export',
    'access arguments' => array(1),
    'page callback' => 'xmlrpc_synch_gui',
    'page arguments' => array(1, $format_handler),
    'title' => 'Node export (' . $format['#title'] . ')',
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
    'file' => 'xmlrpc_synch.pages.inc',
    );
    }
    }
    }
    else {
    $items['node/%node/xmlrpc_synch'] = array(
    'access callback' => 'xmlrpc_synch_access_export',
    'access arguments' => array(1),
    'page callback' => 'xmlrpc_synch_gui',
    'page arguments' => array(1),
    'title' => 'xmlrpc_synch Node export',
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
    'file' => 'xmlrpc_synch.pages.inc',
    );
    } */

  $items['admin/content/xmlrpc_synch'] = array(
    'access arguments' => array('xmlrpc_synch'),
    'page callback' => 'xmlrpc_synch_gui',
    'page arguments' => array(NULL, NULL),
    'title' => 'Node export',
    'type' => MENU_CALLBACK,
    'file' => 'xmlrpc_synch.pages.inc',
  );
  /*
    $items['node/add/xmlrpc_synch'] = array(
    'title' => 'xmlrpc_synch Node export: import',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpc_synch_import_form'),
    'access callback' => 'xmlrpc_synch_access_import',
    'description' => 'Import content using <em>Node export</em>.',
    'file' => 'xmlrpc_synch.pages.inc',
    );
   */
  return $items;
}

function xmlrpc_synch_node_synch($node) {
  if (!empty($node->nid)) {

    drupal_set_message('this node->' . $node->nid);

    $form = drupal_get_form('xmlrpc_synch_node_synch_form', $node->nid);
    $uuid = uuid_get_uuid('node', 'nid', $node->nid);

    drupal_set_message($uuid);

    $new_nid = get_nid_by_uuid($uuid);
    drupal_set_message($new_nid);
//      $new_node=xmlrpc_synch_get_node_by_uuid($uuid);
//      drupal_set_message('new node->'.print_r($new_node,true));

    return $form;
  }
  return TRUE;
}

function xmlrpc_synch_get_node_by_uuid($uuid) {
  $nid = get_nid_by_uuid($uuid);
  $nids = array($nid);
  $result = xmlrpc_synch($nids);

  $res['data'] = $result['output'];

  return $res;
}

function xmlrpc_synch_node_synch_form($form, $nid) {
  $uuid = uuid_get_uuid('node', 'nid', $nid);

  $form = array();
  $form['uuid'] = array(
    '#type' => 'textfield',
    '#value' => $uuid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );
  return $form;
}

function xmlrpc_synch_node_synch_form_submit($form, $form_state) {


  $uuid = $form_state['values']['uuid'];

  $url = variable_get('xmlrpc_synch_server_url', 'http://lukukla.ru/xmlrpc.php');

  $data = xmlrpc($url, 'xmlrpc_synch.get_node_by_uuid', $uuid);
  drupal_set_message('xmlrpc_synch.get_node_by_uuid');
  drupal_set_message(print_r($data, TRUE));

//      $res=xmlrpc_synch_import($data['data']);
//		drupal_set_message('<h1>node updated</h1>');
}

function xmlrpc_synch_types_settings() {
  $query = db_query("select name from {node_type}");
  while ($val = db_fetch_object($query)) {
    $form[$val->type] = array(
      '#type' => 'checkbox',
      '#title' => t($val->name),
    );
  }
  return $form;
}

function xmlrpc_synch_cron_enable_form($form) {
  $form = array();
  $form['cron_enable'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('xmlrpc_synch_cron_enable', ''),
    '#title' => 'Cron_enable',
  );
  $form['server_url'] = array(
    '#type' => 'textfield',
    '#title' => variable_set('xmlrpc_synch_server_url', ''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'submit',
  );
  return $form;
}

function xmlrpc_synch_cron_enable_form_submit($form, $form_state) {

  show_msg($form_state['values']['cron_enable']);
  variable_set('xmlrpc_synch_cron_enable', $form_state['values']['cron_enable']);
  variable_set('xmlrpc_synch_server_url', $form_state['values']['server_url']);
}

function xmlrpc_synch_check_vocabulary() {
  return taxonomy_get_vocabularies();
}

function xmlrpc_synch_check_taxonomy() {

  return taxonomy_get_tree(1);
}

function xmlrpc_synch_get_data($changed = 0, $first = 0, $limit = 5) {

  $res = array();
  if ($last == 0) {
    $last = xmlrpc_synch_get_data_count();
  }

  //$nids_res=db_query_range("SELECT nid FROM {node} where type='product' and changed>=".$changed, $first, $limit);

  $nids_res = db_query_range("SELECT nid ,changed FROM {node} where type='product' and changed>=%d Order by changed ASC", $changed, $first, $limit);

  $res['sql'] = "SELECT nid ,changed FROM {node} where type='product' and changed>=" . $changed . " Order by changed ASC";
  $nids = array();
  while ($val = db_fetch_object($nids_res)) {
    $nids[] = $val->nid;
    $res['changed_log'][] = $val->changed;
    $res['changed'] = $val->changed;
  }

  $result = xmlrpc_synch($nids);

  $res['changed'] = max($res['changed_log']);

  $res['data'] = $result['output'];


  return $res;
}

function xmlrpc_synch_get_last_week_data($one, $two) {
  $last_week = time() - (7 * 24 * 60 * 60);
  show_msg('xmlrpc_synch_get_last_week_data');
  show_msg('123');
  if ($last == 0) {
    $last = xmlrpc_synch_get_data_count();
  }
  $nids_res = db_query_range("SELECT nid FROM {node} where type='product' and changed>=%d", 0, 5);

  $nids = array();
  while ($val = db_fetch_object($nids_res)) {
    $nids[] = $val->nid;
  }

  $result = xmlrpc_synch($nids);
  return $result;
}

function xmlrpc_synch_get_all_views() {
  require_once(drupal_get_path('module', 'views') . '/includes/admin.inc');
  $views = xmlrpc_synch_get_list_of_views();
  $views = xmlrpc_synch_get_all_views_data($views);
  return $views;
}

/*
 * syncronize 
 */
function xmlrpc_synch_batch_syncronize($per_node = FALSE) {


  $last_data_check = variable_get('xmlrpc_synch_last_data_check', 0);

  $count = xmlrpc_synch_get_count($last_data_check);

//	drupal_set_message('last data check ->'.print_r($last_data_check,true));
//	drupal_set_message('Count ->'.$count);

  $url = variable_get('xmlrpc_synch_server_url', 'http://lukukla.ru/xmlrpc.php');

  if ($count >= 10) {

    if ($per_node) {
      $count = 1;
    }
    else {
      $count = 10;
    }

  }
  $data = xmlrpc($url, 'xmlrpc_synch.get_data', $last_data_check, 0, $count);
  $data['changed']++;
  drupal_set_message("changed = > " . $last_data_check);
  drupal_set_message("changed = > " . $data['changed']);

  variable_set('xmlrpc_synch_last_data_check', $data['changed']);

  $res = xmlrpc_synch_import($data['data']);
}

function xmlrpc_synch_get_last_data_client($step = 10) {
  xmlrpc_synch_batch_syncronize();
}

// Simple and artificial: load a node of a given type for a given user
function my_function_1($url, $step = 10, &$context) {
  // The $context array gathers batch context information about the execution (read),
  // as well as 'return values' for the current operation (write)
  // The following keys are provided :
  // 'results' (read / write): The array of results gathered so far by
  //   the batch processing, for the current operation to append its own.
  // 'message' (write): A text message displayed in the progress page.
  // The following keys allow for multi-step operations :
  // 'sandbox' (read / write): An array that can be freely used to
  //   store persistent data between iterations. It is recommended to
  //   use this instead of $_SESSION, which is unsafe if the user
  //   continues browsing in a separate window while the batch is processing.
  // 'finished' (write): A float number between 0 and 1 informing
  //   the processing engine of the completion level for the operation.
  //   1 (or no value explicitly set) means the operation is finished
  //   and the batch processing can continue to the next operation.
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;


    $context['results']['url'] = $url;
    $context['results']['last_data_check'] = variable_get('xmlrpc_synch_last_data_check', 0);

    $count = xmlrpc($url, 'xmlrpc_synch.get_data_count', $context['results']['last_data_check']);
    if ($count > $step) {
      $context['results']['max'] = $step;
    }
    else {
      $context['results']['max'] = $count;
    }
    drupal_set_message("Всего нужно обновить:");
    drupal_set_message($context['results']['max']);
    $context['message'] = "Получение данных с сервера";
  }


  $context['message'] = "Получение количества записей на сервере";
}

function my_function_3(&$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = count($context['results']['data']);
  }

  xmlrpc_synch_import($context['results']['data'][$context['sandbox']['progress']]);
  //$changed = end($context['results']['data'][$context['sandbox']['progress']]);
  //$context['results']['changed']=$changed['changed'];
  $context['sandbox']['progress']++;

  $context['message'] = "Сихронизация данных : " . $context['sandbox']['progress'] . "/" . $context['results']['max'];


  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

// More advanced example: multi-step operation - load all nodes, five by five
function my_function_2(&$context) {

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = $context['results']['max'];
  }


  $limit = 10;

  $data = xmlrpc($context['results']['url'], 'xmlrpc_synch.get_data', $context['results']['last_data_check'], $context['sandbox']['progress'], $limit);
  $context['results']['data'][] = $data['data'];
  $context['results']['sql'] = $data['sql'];
  $context['results']['changed'] = $data['changed'];
  $context['results']['changed_log'] = $data['changed_log'];


  $context['sandbox']['progress'] = $context['sandbox']['progress'] + $limit;
  $context['message'] = "Импорт данных со стороннего сервера " . $context['sandbox']['progress'] . "/" . $context['results']['max'] . "  " . $node->title;
  sleep(20);

  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

//Sample 'finished' callback:


function batch_test_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural($results['max'], 'One post processed.', '@count posts processed.');
  }
  else {
    $message = t('Finished with an error.');
  }

  //drupal_set_message("<pre>".print_r($results,true)."</pre>");
  drupal_set_message($message);

  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Loaded node %title.', array('%title' => $result));
  }

  drupal_set_message('batch all ok');
  $_SESSION['my_batch_results'] = $items;
  variable_set('xmlrpc_synch_last_data_check', $results['changed']);
  $last_data_check = variable_get('xmlrpc_synch_last_data_check', $results['changed']);
  drupal_set_message("Последняя нода обновлялась1 ->" . $results['changed']);
  drupal_set_message("Последняя нода обновлялась2 ->" . $last_data_check);
  drupal_set_message("Последняя нода обновлялась3 ->" . format_date($last_data_check, "medium"));
}

function xmlrpc_synch_get_last_data_server($lastdata) {
  show_msg('xmlrpc_synch_get_last_data');

  if ($last == 0) {
    $last = xmlrpc_synch_get_data_count();
  }
  $nids_res = db_query_range("SELECT nid FROM {node} where type='product' and changed>=" . $lastdata, 0, 2);
  //$nids_res=db_query("SELECT nid FROM {node}");

  $nids = array();
  while ($val = db_fetch_object($nids_res)) {
    $nids[] = $val->nid;
  }

  $result = xmlrpc_synch($nids);

  return $result;
}

function xmlrpc_synch_get_latest_data($guess) {
  if ($guess < 1 || $guess > 10) {
    return xmlrpc_error(1, t('Your guess must be between 1 and 10.'));
  }
  $lucky_number = mt_rand(1, 10);
  if ($guess == $lucky_number) {
    return t('Your number matched!');
  }
  else {
    return t('Sorry, the number was @num.', array('@num' => $lucky_number));
  }
}

function xmlrpc_synch_get_all_data($first = 0, $limit = 0) {
  show_msg('xmlrpc_synch_get_all_data');
  if ($last == 0) {
    $last = xmlrpc_synch_get_data_count();
  }
  $nids_res = db_query_range("SELECT nid FROM {node} where type='product'", $first, $limit);
  //$nids_res=db_query("SELECT nid FROM {node}");

  $nids = array();
  while ($val = db_fetch_object($nids_res)) {
    $nids[] = $val->nid;
  }

  $result = xmlrpc_synch($nids);
  return $result;
}

function show_msg($message) {
  global $debug;
  $debug = FALSE;
  if ($debug) {

    drupal_set_message("<pre>" . print_r($message, TRUE) . "</pre>");
  }
}

function xmlrpc_synch_get_test($guess) {
  if ($guess < 1 || $guess > 10) {
    return xmlrpc_error(1, t('Your guess must be between 1 and 10.'));
  }
  $lucky_number = mt_rand(1, 10);
  if ($guess == $lucky_number) {
    return t('Your number matched!');
  }
  else {
    return t('Sorry, the number was @num.', array('@num' => $lucky_number));
  }
}

/**
 * Implementation of hook_perm().
 */
function xmlrpc_synch_perm() {
  return array('export nodes', 'export own nodes', 'use PHP to import nodes');
}

/**
 * Check access to export a node.
 */
function xmlrpc_synch_access_export($node) {
  global $user;
  if (is_numeric($node)) {
    $node = node_load($node);
  }

  if (function_exists('drush_main')) {
    // Always allow drush to export nodes.
    $access = TRUE;
  }
  else {
    // Check basic role permissions first.
    $access = (user_access('export nodes') || ($user->uid && ($node->uid == $user->uid) && user_access('export own nodes')));
    // Make sure the user can view the original node content.
    $access = $access && node_access('view', $node);
  }

  // Let other modules alter this - for example to only allow some users
  // to export specific nodes or types.
  drupal_alter("xmlrpc_synch_access_export", $access, $node);
  return $access;
}

/*
 * Check access to import a node.
 */

function xmlrpc_synch_access_import($node = NULL) {
  global $user;

  if (function_exists('drush_main')) {
    // Always allow drush to import nodes.
    $access = TRUE;
  }
  elseif (defined('MAINTENANCE_MODE') && MAINTENANCE_MODE == 'install') {
    // During the install phase $user is the Anonymous user; however, in
    // practice $user is performing tasks only granted to the admin user
    // (eg: installing modules, changing site settings).  For this reason
    // it seems sensible to allow this "Anonymous admin" user to import
    // any nodes they wish.
    $access = TRUE;
  }
  else {
    // Check basic role permissions first.
    $access = user_access('use PHP to import nodes');

    if (!is_null($node) && $access) {
      // Check node conditions.
      $access = node_access('create', $node->type) && filter_access($node->format);
    }
  }

  // Let other modules alter this - for example to only allow some users
  // to import specific nodes or types.
  drupal_alter("xmlrpc_synch_access_import", $access, $node);
  return $access;
}

/**
 * Check access to export an array of nodes.
 */
function xmlrpc_synch_access_export_nodes($nodes) {
  // Convert to array if it isn't already.
  if (is_object($nodes)) {
    $nodes = array($nodes);
  }
  foreach ($nodes as &$node) {
    if (!xmlrpc_synch_access_export($node)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Check access to import an array of nodes.
 */
function xmlrpc_synch_access_import_nodes($nodes) {
  // Convert to array if it isn't already.
  if (is_object($nodes)) {
    $nodes = array($nodes);
  }
  foreach ($nodes as &$node) {
    if (!xmlrpc_synch_access_import($node)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Implementation of hook_node_type().
 */
function xmlrpc_synch_node_type($op, $type_obj) {

  switch ($op) {
    case 'delete':
      variable_del('xmlrpc_synch_reset_' . $type_obj->type);
      break;
    case 'update':
      if (!empty($type_obj->old_type) && $type_obj->old_type != $type_obj->type) {
        if (variable_get('xmlrpc_synch_reset_' . $type_obj->old_type, FALSE)) {
          variable_del('xmlrpc_synch_reset_' . $type_obj->old_type);
          variable_set('xmlrpc_synch_reset_' . $type_obj->type, TRUE);
        }
      }
      break;
  }
}

/**
 * Implementation of hook_views_api().
 */
function xmlrpc_synch_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'xmlrpc_synch') . '/views',
  );
}

/**
 * Implementation of hook_node_operations().
 */
function xmlrpc_synch_node_operations() {
  $operations = array();
  if (user_access('export nodes')) {

    $selected_formats = variable_get('xmlrpc_synch_format', array('node_code'));
    if (count(array_filter($selected_formats)) > 1) {
      $format_handlers = xmlrpc_synch_format_handlers();
      foreach ($format_handlers as $format_handler => $format) {
        if ($selected_formats[$format_handler]) {
          $operations['xmlrpc_synch_' . $format_handler] = array(
            'label' => t('Node export') . " (" . $format['#title'] . ")",
            'callback' => 'xmlrpc_synch_bulk_operation',
            'callback arguments' => array($format_handler, NULL),
          );
        }
      }
    }
    else {
      $operations = array(
        'xmlrpc_synch' => array(
          'label' => t('Node export'),
          'callback' => 'xmlrpc_synch_bulk_operation',
          'callback arguments' => array(NULL, NULL),
        ),
      );
    }
  }
  return $operations;
}

/**
 * Callback for use with hook_node_operations().
 */
function xmlrpc_synch_bulk_operation($nodes = NULL, $format = NULL, $delivery = NULL) {
  module_load_include('inc', 'xmlrpc_synch', 'xmlrpc_synch.pages');
  return xmlrpc_synch_gui($nodes, $format, $delivery);
}

/**
 * Export nodes.
 *
 * @param $nids
 *   A node ID or array of node IDs to export.
 * @param $format
 *   The format to use for export.
 * @param $msg_t
 *   Function used to translate.
 * @return
 *   An array with keys 'success' which is a boolean value representing whether
 *   the export was successful and 'output' which contains the code string or an
 *   array of translated error messages to be shown to the user.
 */
function xmlrpc_synch($nids, $format = NULL, $msg_t = 't') {
  global $user;

  // Make $nids an array if it isn't.
  if (is_numeric($nids)) {
    $nids = array($nids);
  }
  elseif (is_object($nids)) {
    $nids = array($nids->nid);
  }

  $nodes = array();
  foreach ($nids as $nid) {
    $original_node = node_load($nid);

    /* if (!xmlrpc_synch_access_export($original_node)) {
      // Halt exporting.
      $error = $msg_t("You do not have permission to perform a Node export on one or more of these nodes.  No nodes exported.");
      return array(
      'success' => FALSE,
      'output' => array($error),
      );
      } */

    $node = xmlrpc_synch_prepare_node($original_node);

    $nodes[] = $node;
  }

  // Get the node code from the format handler
  $format_handlers = xmlrpc_synch_format_handlers();
  $format_handler = $format ? $format : reset(variable_get('xmlrpc_synch_format', array('node_code')));
  if (!isset($format_handlers[$format_handler])) {
    $format_handler = 'node_code';
  }

  // Let other modules do special fixing up.
  drupal_alter('xmlrpc_synch', $nodes, 'export', $format_handler);

  // If any nodes are set to FALSE, then an error was triggered in another module.
  // Currently modules doing this should also leave a watchdog warning.
  if (in_array(FALSE, $nodes)) {
    // Halt exporting.
    $error = $msg_t('An error occurred when processing nodes, please check your logs.  No nodes exported.');
    return array(
      'success' => FALSE,
      'output' => array($error),
    );
  }

  $code_string = module_invoke(
    $format_handlers[$format_handler]['#module'], 'xmlrpc_synch', $nodes, $format_handler
  );

  // Let modules modify the node code.
  drupal_alter('xmlrpc_synch_encode', $code_string, $nodes, $format_handler);

  return array(
    'success' => TRUE,
    'output' => $code_string,
    'format' => $format_handler,
  );
}

function xmlrpc_synch_make_changes($node, $original_node) {
  if (variable_get('xmlrpc_synch_make_changes_title', 0) == 1) {
    $node->title = $original_node->title;
    $node->page_title = $original_node->page_title;
  }

  $node->field_tel[0]['value'] = "123456";
  if (isset($node->field_tel)) {
    $node->field_tel[0]['value'] = "123456";
  }

  if (isset($node->field_dostavka)) {
    $node->field_dostavka[0]['value'] = '';
  }


  $sitename = variable_get('xmlrpc_synch_sitename', 'lusergerostkukla.ru');

  //$node->body=srt_replace('лукукла',$sitename,$node->body);
  //$node->teaser=srt_replace('лукукла',$sitename,$node->teaser);
  drupal_set_message('make changes');

  if ($node->type == 'product') {
    //$body=variable_get('xmlrpc_synch_body','');
    //$node->body=$body;


    $body = variable_get('xmlrpc_synch_make_changes_body', 'body');
    if (!empty($body)) {
      $node->body = $body;
      $node->teaser = $body;
    }
    //change email

    $email = variable_get('xmlrpc_synch_make_changes_email', 'email@email.ru');
    if (!empty($email)) {
      $node->field_email['0']['value'] = $email;
    }
    //change telephone

    $telephone = variable_get('xmlrpc_synch_make_changes_telephone', '+xxx xx xxx-xx-xx');
    if (!empty($telephone)) {
      $node->field_tel['0']['value'] = $telephone;
    }
    //delete discount

    $node->field_old_price[0]['value'] = NULL;

    // Pod Zakaz.
    // logic was moved to upper function
    // Now. if node was new. it's satus will be 'Pod zakaz'
    // If node was already created it's status will be the same as before.

    // change price
    $multiplier = variable_get('xmlrpc_synch_make_changes_multiplier', 1);

    //drupal_set_message($multiplier);
    if (!empty($multiplier) && ($multiplier > 1)) {
      ///drupal_set_message("price was changed");
      $node->field_baseprice[0]['value'] = $original_node->field_baseprice[0]['value'] * $multiplier;
      $node->field_rent_int[0]['value'] = $original_node->field_rent_int[0]['value'] * $multiplier;
    }
    else {
      //drupal_set_message("price don't change");
      //drupal_set_message($node->field_baseprice[0]['value']);
      //drupal_set_message($node->field_rent_int[0]['value']);
    }
  }
  else {
    $node->body = str_replace('http://lusergerostkukla.ru', '', $node->body);
    $node->body = str_replace('http://www.lusergerostkukla.ru', '', $node->body);
    $node->teaser = $node->body;
  }

  //$teaser=variable_get('xmlrpc_synch_teaser','??????? ??????????');


  return $node;
}

/**
 * Prepare a single node during export.
 */
function xmlrpc_synch_prepare_node(&$original_node) {
  // Create UUID if it's not there.
  // Currently this uses a module_exists() until UUID becomes a dependency.

  if (module_exists('uuid')) {
    if (!uuid_get_uuid('node', 'nid', $original_node->nid)) {
      $original_node->uuid = uuid_set_uuid('node', 'nid', $original_node->nid);
      // Save it so future node exports are consistent.
      node_save($original_node);
      show_msg('node_save becouse uuid');
    }
  }

  $node = drupal_clone($original_node);

  // Fix taxonomy array
  if (isset($node->taxonomy) && count($node->taxonomy)) {
    $vocabularies = taxonomy_get_vocabularies();
    $new_taxonomy = array();
    foreach ($node->taxonomy as $term) {
      // Free-tagging vocabularies need a different format
      if ($vocabularies[$term->vid]->tags) {
        $new_taxonomy['tags'][$term->vid][] = $term->name;
      }
      else {
        $new_taxonomy[$term->vid][$term->tid] = $term->tid;
      }
    }
    if (isset($new_taxonomy['tags']) && count($new_taxonomy['tags'])) {
      // Comma seperate the tags
      foreach ($new_taxonomy['tags'] as $vid => $tags) {
        $new_taxonomy['tags'][$vid] = implode(', ', $tags);
      }
    }
    $node->taxonomy = $new_taxonomy;
  }

  // Fix menu array
  $node->menu = xmlrpc_synch_get_menu($original_node);

  $node = xmlrpc_synch_remove_recursion($node);

  // Let other modules do special fixing up.
  drupal_alter('xmlrpc_synch_node', $node, $original_node, 'export');

  return $node;
}

/**
 *  Check if all types in the import exist.
 *
 * @return
 *    TRUE if all types exist, otherwise an array of missing type names.
 */
function xmlrpc_synch_import_types_check($nodes) {
  $missing_types = array();
  foreach ($nodes as $node) {
    if (node_get_types('name', $node) == FALSE) {
      $missing_types[$node->type] = $node->type;
    }
  }
  return (!empty($missing_types) ? $missing_types : TRUE);
}

/**
 * Import Function
 *
 * @param $code_string
 *   The string of export code.
 * @param $msg_t
 *   Function used to translate.
 * @return
 *   An array with keys 'success' which is a boolean value representing whether
 *   the import was successful and 'output' which contains an array of
 *   translated strings to be shown to the user as messages.
 */
function xmlrpc_synch_check_types($origin_types = array()) {

  $types = content_copy_types();
  $arr = array();
  $return = array();
  foreach ($types as $type_name => $type) {
    $result = array();

    $result['type_name'] = $type_name;


    $result['macro'] = xmlrpc_synch_get_types($type_name);

    $arr = array();
    //$return[]=content_copy_export($result);
    $arr['type_name'] = $type_name;
    $arr['macro'] = content_copy_export($result);
    $return[] = array('values' => $arr);
  }

  return $return;
}

function xmlrpc_synch_get_types($type_name, $new = FALSE) {

  $return = array();
  $exportable_fields = content_copy_fields($type_name);

  $return['type_name'] = $type_name;

  foreach ($exportable_fields as $val) {
    $return['fields'][$val] = $val;
  }
  $return['step'] = 2;

  $data = array();


  return $return;
}

/*
  import node
 */

function xmlrpc_synch_import($code_string, $msg_t = 't') {
  drupal_set_message('import node');
  // Early check to avoid letting hooligans and the elderly pass data to the
  // eval() function call.
  /* if (!xmlrpc_synch_access_import()) {
    $error = $msg_t(
    'You do not have permission to import any nodes.'
    );
    return array(
    'success' => FALSE,
    'output' => array($error),
    );
    } */

  // Allow modules to manipulate the $code_string.
  drupal_alter('xmlrpc_synch_decode', $code_string);

  // Pass the string to each format handler until one returns something useful.
  $format_handlers = xmlrpc_synch_format_handlers();
  $nodes = array();
  $used_format = "";
  foreach ($format_handlers as $format_handler => $format) {
    $nodes = module_invoke(
      $format_handlers[$format_handler]['#module'], 'xmlrpc_synch_import', $code_string
    );
    if (!empty($nodes)) {
      $used_format = $format_handler;
      break;
    }
  }

  if (isset($nodes['success']) && !$nodes['success']) {
    // Instead of returning nodes, the format handler returned an error array.
    // Translate the errors and return them.
    foreach ($nodes['output'] as $key => $output) {
      $nodes['output'][$key] = $msg_t($output);
    }
    return array(
      'success' => FALSE,
      'output' => $nodes['output'],
    );
  }

  if ($used_format == "") {
    $error = $msg_t(
      'Node export was unable to recognize the format of the supplied code.  No nodes imported.'
    );
    return array(
      'success' => FALSE,
      'output' => array($error),
    );
  }

  $nodes = xmlrpc_synch_restore_recursion($nodes);

  $types_exist = xmlrpc_synch_import_types_check($nodes);


  if ($types_exist !== TRUE) {
    // get types from site
    xmlrpc_synch_types_remote_get($types_exist);
    // There was a problem with the content types check.


    $error = $msg_t(
      'Error encountered during import.  Node types unknown on this site: %t.  No nodes imported.', array('%t' => implode(", ", $types_exist))
    );
    return array(
      'success' => FALSE,
      'output' => array($error),
    );
  }
  /*
   *
    if (!xmlrpc_synch_access_import_nodes($nodes)) {
    // There was a problem with permissions.
    $error = $msg_t(
    'You do not have permission to perform a Node export: import on one or more of these nodes.  No nodes imported.'
    );
    return array(
    'success' => FALSE,
    'output' => array($error),
    );
    }
   */
  $count = 0;
  $total = count($nodes);
  // Let other modules do special fixing up.
  drupal_alter('xmlrpc_synch', $nodes, 'import', $used_format);
  $new_nodes = array();
  $new_nid = 0;
  $messages = array();
  //	drupal_set_message("<pre>".print_r($nodes,true)."</pre>");
  foreach ($nodes as $original_node) {
    $nid = NULL;
    //drupal_set_message("<pre>".print_r($original_node->field_pic600x900,true)."</pre>");

    $nid = uuid_get_nid_byuuid('node', $original_node->uuid);
    //xmlrpc_synch_file_url
    //drupal_set_message(print_r($original_node->field_pic600x900,true));
    //drupal_set_message("<pre>".print_r($original_node,true)."</pre>");
    if ($nid) {
      $node = node_load($nid);
      //$node=xmlrpc_synch_node_update($node,$original_node);
    }
    else {
      drupal_set_message("nid  don't exists");
      $node = $original_node;
      // Set status 'pod zakaz'.
      $node->field_stock[0]['value'] = 228;
      $node->nid = NULL;

      //drupal_alter('xmlrpc_synch_node', $node, $original_node, 'import');
    }


    foreach ($original_node->field_pic600x900 as $key => $file) {

      $file_url = $file['xmlrpc_synch_file_url'];

      $file_path = $file['xmlrpc_synch_file_url'];

      if (file_exists($node->field_pic600x900[$key]['filepath'])) {

        drupal_set_message($node->title . ' файл уже существует.' . $node->field_pic600x900[$key]['filepath']);

      }
      else {
        drupal_set_message('filename-> ' . $file['filename']);
        drupal_set_message($node->title . ' файл не существует существует.' . $node->field_pic600x900[$key]['filepath']);
        drupal_set_message('<h2>Файл на главном сервере ' . $file['xmlrpc_synch_file_url'] . ' </h2>');

        $file['filepath'] = str_replace('#FILES_DIRECTORY_PATH#', 'sites/default/files', $file['filepath']);

        $node->field_pic600x900[$key]['filepath'] = str_replace('#FILES_DIRECTORY_PATH#', 'sites/default/files', $file['filepath']);

        drupal_set_message($node->title . ' файл ' . $file['filename'] . ' копируем с ' . $file['xmlrpc_synch_file_url'] . '  в ' . $file['filepath']);

        copy($file['xmlrpc_synch_file_url'], $file['filepath']);

        // file_copy($file['xmlrpc_synch_file_url'], 0, FILE_EXISTS_REPLACE);

        $file_field = field_file_save_file($file['filepath'], array(), file_directory_path(), 1);
        drupal_set_message(print_r($file_field, TRUE));
        $node->field_pic600x900[$key] = $file_field;
        drupal_set_message($node->title . ' файл по пути.' . $node->field_pic600x900[$key]['filepath']);
        global $base_url;


        if (!file_exists($file['filepath'])) {
          drupal_set_message('<h2>Файл так и не скопирован</h2>');
          /*      drupal_set_message('<h2>Файл на главном сервере '.$file['xmlrpc_synch_file_url'].' </h2>');
            drupal_set_message('<h2>Файл на текущем сайте '.$base_url.'/'.$file['filepath'].' </h2>');
            // file_copy($file['xmlrpc_synch_file_url'], '', FILE_EXISTS_REPLACE);
            if (file_exists($file['filepath'])){
            drupal_set_message('<h2>Изображение!! <img src="'.$base_url.'/'.$file['filepath'].'"> </h2>');
            }
            copy($file['xmlrpc_synch_file_url'],'sites/default/files/'.$file['filename']);
            //                                        copy($file['xmlrpc_synch_file_url'],$file['filepath']);
            $file_field=field_file_save_file($file['filepath'], array(), file_directory_path(), NULL);

            $node->field_pic600x900[$key]['fid']=$file_field['fid'];
            drupal_set_message($node->title.' файл по пути.'.$node->field_pic600x900[$key]['filepath']);
           */
        }
        else {
          drupal_set_message('<h2>Файл всеже скопирован</h2>');
        }
        drupal_set_message("<img src='" . $base_url . "/" . $node->field_pic600x900[$key]['filepath'] . "' height='200'/>");
        drupal_set_message("<pre>" . print_r($node->field_pic600x900, TRUE) . "</pre>");
      }
    }

    $node = xmlrpc_synch_make_changes($node, $original_node);

    //show_msg($node);
    // Let other modules do special fixing up.
    //$original_node->field_pic600x900[0]['filepath']='sites/files/default/devochka-russkaya_600x900.jpg';
    //$node->field_pic600x900=$original_node->ield_pic600x900;
    //drupal_set_message("xmlrpc_synch_save");
    //node_save($node);
    //drupal_set_message("<pre>".print_r($node,true)."</pre>");
    xmlrpc_synch_save($node);
    $new_nodes[] = $node;
    $messages[] = $msg_t("Imported node !nid: !node", array(
      '!nid' => $node->nid,
      '!node' => l($node->title, 'node/' . $node->nid)
    ));

    $count++;
  }

  //drupal_alter('xmlrpc_synch', $new_nodes, 'after import', $used_format);
  $messages[] = $msg_t("!count of !total nodes were imported.  Some values may have been reset depending on Node export's configuration.", array(
    '!total' => $total,
    '!count' => $count
  ));
  foreach ($messages as $message) {
    drupal_set_message($message);
  }
  // Clear the page and block caches.
  cache_clear_all();

  return array(
    'success' => TRUE,
    'output' => $messages,
  );
}

function xmlrpc_synch_node_update($node, $on) {


  $node->title = $on->title;
  $node->body = " Кукла " . $on->title . " подарит вам счастье";
  $node->teaser = $node->teaser;
  if ($node->type == 'product') {
    $node->field_rent_int = $on->field_rent_int;
  }

  return $node;
}

/**
 * Save a node object into the database.
 *
 * A modified version of node_save().
 */
function xmlrpc_synch_save(&$node) {
  // Let modules modify the node before it is saved to the database.
  node_invoke_nodeapi($node, 'presave');
  global $user;

  $node->is_new = FALSE;

  // Apply filters to some default node fields:
  if (empty($node->nid)) {
    show_msg('insert new node');
    // Insert a new node.
    $node->is_new = TRUE;

    // When inserting a node, $node->log must be set because
    // {node_revisions}.log does not (and cannot) have a default
    // value.  If the user does not have permission to create
    // revisions, however, the form will not contain an element for
    // log so $node->log will be unset at this point.
    if (!isset($node->log)) {
      $node->log = '';
    }

    // For the same reasons, make sure we have $node->teaser and
    // $node->body.  We should consider making these fields nullable
    // in a future version since node types are not required to use them.
    if (!isset($node->teaser)) {
      $node->teaser = '';
    }
    if (!isset($node->body)) {
      $node->body = '';
    }
  }
  elseif (!empty($node->revision)) {
    $node->old_vid = $node->vid;
  }
  else {
    // When updating a node, avoid clobberring an existing log entry with an empty one.
    if (empty($node->log)) {
      unset($node->log);
    }
  }

  // Set some required fields:
  if (empty($node->created)) {
    $node->created = time();
  }

  // The update of the changed value is forced in the original node_save().
  if (empty($node->changed)) {
    $node->changed = time();
  }

  $node->timestamp = time();
  $node->format = isset($node->format) ? $node->format : FILTER_FORMAT_DEFAULT;

  // Generate the node table query and the node_revisions table query.
  if ($node->is_new) {
    _node_save_revision($node, $user->uid);
    drupal_write_record('node', $node);
    db_query('UPDATE {node_revisions} SET nid = %d WHERE vid = %d', $node->nid, $node->vid);
    $op = 'insert';
    show_msg('insert');
  }
  else {
    drupal_write_record('node', $node, 'nid');
    if (!empty($node->revision)) {
      _node_save_revision($node, $user->uid);
      db_query('UPDATE {node} SET vid = %d WHERE nid = %d', $node->vid, $node->nid);
      show_msg('revision not empty');
    }
    else {
      _node_save_revision($node, $user->uid, 'vid');
      show_msg('revision empty');
      show_msg($node);
    }
    $op = 'update';
    show_msg('update');
  }


  // Call the node specific callback (if any).
  node_invoke($node, $op);
  node_invoke_nodeapi($node, $op);

  // Update the node access table for this node.
  node_access_acquire_grants($node);
}

/**
 * Prepare a clone of the node during import.
 */
function xmlrpc_synch_node_clone($original_node) {
  global $user;

  $node = drupal_clone($original_node);

  $node->nid = NULL;
  $node->vid = NULL;
  $node->name = $user->name;
  $node->uid = $user->uid;

  if (variable_get('xmlrpc_synch_reset_created_' . $node->type, TRUE)) {
    $node->created = NULL;
  }

  if (variable_get('xmlrpc_synch_reset_changed_' . $node->type, TRUE)) {
    $node->changed = NULL;
  }

  if (variable_get('xmlrpc_synch_reset_menu_' . $node->type, TRUE)) {
    $node->menu = NULL;
  }
  if (isset($node->path)) {
    //unset($node->path['pid']);
  }
  if (module_exists('pathauto')) {
    // Prevent pathauto from creating a new path alias.
    $node->pathauto_perform_alias = FALSE;
  }


  if (variable_get('xmlrpc_synch_reset_book_mlid_' . $node->type, TRUE) && isset($node->book['mlid'])) {
    $node->book['mlid'] = NULL;
  }

  $node->files = array();

  if (variable_get('xmlrpc_synch_reset_' . $node->type, FALSE)) {
    $node_options = variable_get('node_options_' . $node->type, array(
      'status',
      'promote'
    ));
    // Fill in the default values.
    foreach (array(
               'status',
               'moderate',
               'promote',
               'sticky',
               'revision'
             ) as $key) {
      $node->$key = in_array($key, $node_options);
    }
  }


  return $node;
}

function xmlrpc_synch_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  show_msg("<h1>" . $op . "</h1>");
  if ($op == 'presave' || $op == "insert" || $op == "update") {
    //show_msg($node);
    //show_msg($node->uuid);
    //show_msg($node->revision_uuid);
  }
  if ($op == 'delete') {

  }
}

/**
 * Create a new menu entry with title, parent and weight exported from
 * another nodes menu. Returns NULL if the node has no menu title.
 */
function xmlrpc_synch_get_menu($node) {
  // This will fetch the existing menu item if the node had one.
  node_invoke_nodeapi($node, 'prepare');

  // Only keep the values we care about.
  if (!empty($node->menu)) {

    // Store a copy of the old menu
    $old_menu = $node->menu;

    // Now fetch the defaults for a new menu entry.
    $node = NULL;
    node_invoke_nodeapi($node, 'prepare');

    // Make a list of values to attempt to copy.
    $menu_fields = array(
      'link_title',
      'plid',
      'menu_name',
      'weight',
      // These should import properly always.
      'hidden',
      'expanded',
      'has_children',
      // These will only import properly on 'Save as a new node then edit' imports.
    );

    // Copy those fields from the old menu over the new menu defaults.
    foreach ($menu_fields as $menu_field) {
      $node->menu[$menu_field] = $old_menu[$menu_field];
    }

    // Return the menu.
    return $node->menu;
  }
}

/**
 * Remove recursion problem from an object or array.
 */
function xmlrpc_synch_remove_recursion($o) {
  static $replace;
  if (!isset($replace)) {
    $replace = create_function(
      '$m', '$r="\x00{$m[1]}ecursion_export_node_";return \'s:\'.strlen($r.$m[2]).\':"\'.$r.$m[2].\'";\';'
    );
  }
  if (is_array($o) || is_object($o)) {
    $re = '#(r|R):([0-9]+);#';
    $serialize = serialize($o);
    if (preg_match($re, $serialize)) {
      $last = $pos = 0;
      while (FALSE !== ($pos = strpos($serialize, 's:', $pos))) {
        $chunk = substr($serialize, $last, $pos - $last);
        if (preg_match($re, $chunk)) {
          $length = strlen($chunk);
          $chunk = preg_replace_callback($re, $replace, $chunk);
          $serialize = substr($serialize, 0, $last) . $chunk . substr($serialize, $last + ($pos - $last));
          $pos += strlen($chunk) - $length;
        }
        $pos += 2;
        $last = strpos($serialize, ':', $pos);
        $length = substr($serialize, $pos, $last - $pos);
        $last += 4 + $length;
        $pos = $last;
      }
      $serialize = substr($serialize, 0, $last) . preg_replace_callback($re, $replace, substr($serialize, $last));
      $o = unserialize($serialize);
    }
  }
  return $o;
}

/**
 * Restore recursion to an object or array.
 */
function xmlrpc_synch_restore_recursion($o) {
  return unserialize(
    preg_replace(
      '#s:[0-9]+:"\x00(r|R)ecursion_export_node_([0-9]+)";#', '\1:\2;', serialize($o)
    )
  );
}

/**
 * Get a list of possible format handlers (other than the default).
 *
 * @return
 *   An array of format handlers from hook implementations.
 * @see hook_xmlrpc_synch_format_handlers()
 */
function xmlrpc_synch_format_handlers() {
  static $format_handlers;

  // Get default format handler.
  module_load_include('inc', 'xmlrpc_synch', 'xmlrpc_synch.node_code');

  if (empty($format_handlers)) {
    $format_handlers = module_invoke_all('xmlrpc_synch_format_handlers');
  }
  return $format_handlers;
}

// Remove once http://drupal.org/node/858274 is resolved.
if (!function_exists('uuid_set_uuid')) {

  /**
   * API function to set the UUID of an object based on its serial ID.
   *
   * @param $table
   *   Base table of the object. Currently, one of node, revision_revisions,
   *   users, vocabulary or term_data.
   * @param $key
   *   The name of the serial ID column.
   * @param $serial_id
   *   The serial ID of the object.
   * @param $uuid
   *   Optional UUID.  If omitted, a UUID will be generated.
   * @return
   *   The UUID on success, FALSE if the uuid provided is not valid.
   */
  function uuid_set_uuid($table, $key, $serial_id, $uuid = FALSE) {
    if (empty($uuid)) {
      $uuid = uuid_uuid();
    }

    if (!uuid_is_valid($uuid)) {
      return FALSE;
    }

    $uuid_table = 'uuid_' . $table;
    db_query("UPDATE {" . $uuid_table . "} SET uuid = '%s' WHERE " . $key . " = %d", $uuid, $serial_id);
    if (!db_affected_rows()) {
      @db_query("INSERT INTO {" . $uuid_table . "} (" . $key . ", uuid) VALUES (%d, '%s')", $serial_id, $uuid);
    }

    return $uuid;
  }

}

// Remove once http://drupal.org/node/858274 is resolved.
if (!function_exists('uuid_get_uuid')) {

  /**
   * API function to get the UUID of an object based on its serial ID.
   *
   * @param $table
   *   Base table of the object. Currently, one of node, revision_revisions,
   *   users, vocabulary or term_data.
   * @param $key
   *   The name of the serial ID column.
   * @param $id
   *   The serial ID of the object.
   * @return
   *   The UUID of the object, or FALSE if not found.
   */
  function uuid_get_uuid($table, $key, $id) {
    $uuid_table = 'uuid_' . $table;
    return db_result(db_query("SELECT uuid FROM {{$uuid_table}} WHERE $key = %d", $id));
  }

  function get_nid_by_uuid($uuid) {
    $uuid_table = 'uuid_node';
    return db_result(db_query("SELECT `nid` FROM `uuid_node` WHERE `uuid` LIKE '%d'", $uuid));
  }

}

/**
 * Implementation of hook_form_alter().
 */
function xmlrpc_synch_file_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'xmlrpc_synch_form') {
    // TODO: Add extra options to the export form?
    show_msg('xmlrpc_synch_form');
  }
  elseif ($form_id == 'xmlrpc_synch_settings') {
    show_msg('xmlrpc_synch_settings');
    show_msg(drupal_get_path('module', 'xmlrpc_synch_file'));

    drupal_add_js(drupal_get_path('module', 'xmlrpc_synch_file') . '/js/xmlrpc_synch_file_admin.js');

    // Remove buttons, but store for reattaching.
    $buttons = $form['buttons'];
    unset($form['buttons']);

    $form['file'] = array(
      '#type' => 'fieldset',
      '#title' => t('Export files'),
    );

    $types = node_get_types('names');
    $form['file']['xmlrpc_synch_file_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Files exported for content types'),
      '#default_value' => variable_get('xmlrpc_synch_file_types', array()),
      '#options' => $types,
      '#description' => t('Which content types should export file attachments and uploads?'),
    );

    $textarea_delivery = $form['basic']['xmlrpc_synch_node_code']['#default_value'];

    $mode_message_display = ($textarea_delivery != 'file');

    $form['file']['xmlrpc_synch_file_mode'] = array(
      '#type' => 'radios',
      '#title' => t('File export mode'),
      '#default_value' => variable_get('xmlrpc_synch_file_mode', 'inline'),
      '#options' => array(
        'inline' => t('Inside export code (base 64 encoded string)'),
        'local' => t('Local file export'),
        'remote' => t('Remote file export, URL')
      ),
      '#description' => t('Should file exports be inline inside the export code, a local path to the file or a URL?  "Inside export code" is the easiest option to use, local and remote modes are more useful for power users.  <em>NOTE: Remote mode only works with a public files directory.</em>'),
      '#prefix' => '<div id="node-export-file-mode-message"><div class="message warning" style="display: ' . ($mode_message_display ? 'block' : 'none') . ';">' . t('Warning, "Inside export code (base 64 encoded string)" mode generates much larger exports.  For this reason it is recommended you set the export code delivery methods to "Text file download".') . '</div></div>',
    );

    $form['file']['xmlrpc_synch_file_assets_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Export FileField assets path'),
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => variable_get('xmlrpc_synch_file_assets_path', ''),
      '#description' => t(
        'Optionally, copy file uploads to this path when the node is exported.
The primary advantage of this is to divert exported files into a
safe location so they can be commmitted to source control (eg: Subversion,
CVS, git).  <em>Tip: For install profile developers, setting this
path to <code>profiles/my_profile/xmlrpc_synch_assets</code> may be
useful.</em>'
      ),
      '#required' => FALSE,
    );

    // Reattach buttons.
    $form['buttons'] = $buttons;
  }
}

/**
 * Abstract hook_xmlrpc_synch_node_alter() used by filefield, upload, and image.
 */
function xmlrpc_synch_file_alter_filefield(&$node, $original_node, $op, $attribute_filter_callback) {
  drupal_set_message('file alter filefield');

  $assets_path = variable_get('xmlrpc_synch_file_assets_path', '');
  $export_mode = variable_get('xmlrpc_synch_file_mode', 'inline');
  $export_mode = 'remote';
  /*
    switch ($export_mode) {
    case 'local':
    $export_var = 'xmlrpc_synch_file_path'; break;
    case 'remote':
    $export_var = 'xmlrpc_synch_file_url';  break;
    default:
    case 'inline':
    $export_var = 'xmlrpc_synch_file_data'; break;
    } */
  $export_var = 'xmlrpc_synch_file_url';

  if ($op == 'export') {
    if (xmlrpc_synch_file_check_assets_path($export_mode, $assets_path) === FALSE) {
      // Don't continue if the assets path is not ready
      return;
    }
  }

  foreach ($node as $attr => $value) {
    $field = & $node->$attr;

    // Filter this attribute by callback
    if (!call_user_func_array($attribute_filter_callback, array(
      'attribute' => $attr,
      'field' => $field
    ))
    ) {
      continue;
    }

    // Strip off anything that isn't a file, if no files are found skip to
    // the next attribute
    if (($files = xmlrpc_synch_file_check_files($field)) == FALSE) {
      continue;
    }


    foreach ($files as $i => $file) {
      // When exporting a node ...
      if ($op == 'export') {
        if (!isset($file->filepath) || !is_file($file->filepath)) {
          show_msg(t("CCK file upload found on node, but doesn't exist on disk? '!path'", array('!path' => $file->filepath)), 'error');
          continue;
        }

        $export_data = xmlrpc_synch_file_get_export_data($file, $export_mode, $assets_path);

        if (is_object($field[$i])) {
          $field[$i]->{$export_var} = $export_data;
          $field[$i]->fid = NULL;
          xmlrpc_synch_file_clean_local_file_path($field[$i]->filepath);
        }
        elseif (is_array($field[$i])) {
          $field[$i][$export_var] = $export_data;
          $field[$i]['fid'] = NULL;
          xmlrpc_synch_file_clean_local_file_path($field[$i]['filepath']);
        }
      }
      // When importing a node ...
      elseif ($op == 'import') {
        $result = xmlrpc_synch_file_import_file($file);

        // The file was saved successfully, update the file field (by reference)
        if ($result == TRUE && isset($file->fid)) {
          if (is_object($field[$i])) {
            $field[$i]->fid = $file->fid;
            $field[$i]->filepath = $file->filepath;
          }
          elseif (is_array($field[$i])) {
            $field[$i]['fid'] = $file->fid;
            $field[$i]['filepath'] = $file->filepath;
          }
        }
      }
    }
  }
}

/**
 * Implementation of hook_xmlrpc_synch_node_alter().
 *
 * Handles importing and exporting CCK FileFields
 *
 * @param $module
 *   An extra paramater that allows this hook implementation to do work
 *   for other modules that are very similar to filefield (eg: upload).
 */
function xmlrpc_synch_file_xmlrpc_synch_node_alter(&$node, $original_node, $op) {

  if ($op == 'export') {
    $types = array_filter(variable_get('xmlrpc_synch_file_types', array()));
    if (!in_array($node->type, $types)) {
      return;
    }
  }
  show_msg('file synch node alter');
  show_msg($op);

  xmlrpc_synch_file_alter_filefield($node, $original_node, $op, 'xmlrpc_synch_file_filefield_filter');
  xmlrpc_synch_file_alter_upload($node, $original_node, $op);
  xmlrpc_synch_file_alter_image($node, $original_node, $op);
}

/**
 * Upload module field filter callback used in xmlrpc_synch_file_alter_filefield().
 */
function xmlrpc_synch_file_upload_filter($attribute, $field) {
  return ($attribute == 'files' && is_array($field));
}

/**
 * Handles importing and exporting CCK FileFields
 */
function xmlrpc_synch_file_alter_upload(&$node, $original_node, $op) {
  if (isset($original_node->files) && count($original_node->files) > 0) {
    // Re-attach files, xmlrpc_synch module strips them off
    $node->files = $original_node->files;

    if ($op == 'export') {
      // Mark all file uploads as "new", this is required by the upload module
      foreach ($node->files as $fid => $file) {
        $node->files[$fid]->new = TRUE;
      }
    }

    xmlrpc_synch_file_alter_filefield($node, $original_node, $op, 'xmlrpc_synch_file_upload_filter');
  }
}

/**
 * FileField field filter callback used in xmlrpc_synch_file_alter_filefield().
 */
function xmlrpc_synch_file_filefield_filter($attribute, $field) {
  return (drupal_substr($attribute, 0, 6) == 'field_' && is_array($field));
}

/**
 * Handles importing and exporting photos uploaded to image nodes.  This
 * is more complicated than for filefields or upload attachments because
 * the image module doesn't attach the file objects for images to the node.
 * To get around this we create a special 'image_export_files' attribute
 * on the node.  This will be stripped off when the node is imported.
 */
function xmlrpc_synch_file_alter_image(&$node, $original_node, $op) {
  // Only act on image nodes
  if ($node->type != 'image') {
    return;
  }

  if ($op == 'export') {
    // Only export the original image, the build-derivatives attribute
    // is set later to rebuild all image sizes on the import side
    $result = db_query(
      "SELECT f.*
FROM {image} AS i
INNER JOIN {files} AS f ON i.fid = f.fid
WHERE i.nid = %d AND f.filename = '%s'", $original_node->nid, IMAGE_ORIGINAL
    );
    $node->image_export_files = array(db_fetch_object($result));

    // Unset the images array, this will cause problems during the
    // 'prepopulate' step during import
    $node->images = array();

    $node->rebuild_images = TRUE;
  }

  xmlrpc_synch_file_alter_filefield($node, $original_node, $op, 'xmlrpc_synch_file_image_filter');

  // The file objects have been saved to the database but the rest of the
  // image tables have not been hooked up yet.
  if (($op == 'import') && !empty($node->image_export_files)) {
    $node->images = array();

    // Create the images array based on the imported files
    foreach ($node->image_export_files as $file) {
      $node->images[$file->filename] = $file->filepath;
    }
  }
}

/**
 * Upload module field filter callback used in xmlrpc_synch_file_alter_filefield().
 */
function xmlrpc_synch_file_image_filter($attribute, $field) {
  return ($attribute == 'image_export_files' && is_array($field));
}

/**
 * Ensure the assets path is exists and is writeable.
 *
 * @param $export_mode
 *   The files export mode, 'local' or 'remote'
 * @param $assets_path
 *   The assets path, should be empty ('') if assets are not supposed to
 *   be copied.
 * @return TRUE or FALSE or NULL
 *   TRUE  if the assets path is ready, and files should be copied there
 *   FALSE if the assets path is not ready, and files should be copied there
 *   NULL  if files shouldn't be copied to the assets path
 */
function xmlrpc_synch_file_check_assets_path($export_mode, $assets_path) {
  // If files are supposed to be copied to the assets path.
  if ($export_mode == 'local' && $assets_path) {
    // Ensure the assets path is created
    if (!is_dir($assets_path) && mkdir($assets_path, 0777, TRUE) == FALSE) {
      show_msg(t("Could not create assets path! '!path'", array('!path' => $assets_path)), 'error');
      return FALSE;
    }

    // Ensure it is writable
    if (!is_writable($assets_path)) {
      show_msg(t("Assets path is not writable! '!path'", array('!path' => $assets_path)), 'error');
      return FALSE;
    }

    return TRUE;
  }

  return NULL;
}

/**
 * Checks an array of files, removing any that are invalid.
 *
 * @return $files or FALSE
 *   Returns an array of $files objects, or FALSE if there are no files.
 */
function xmlrpc_synch_file_check_files($original_files) {
  $files = array();

  foreach ($original_files as $i => $file) {
    $file = (object) $file;

    if (isset($file->filepath)) {
      // NOTE: Preserving the $i key is important for later manipulation
      $files[$i] = $file;
    }
  }

  return (count($files) > 0) ? $files : FALSE;
}

/**
 * Handles the file copying parts and local/remote parts of the file export.
 *
 * @param $file
 *   The file object to handle.
 * @param $export_mode
 *   The mode, 'inline' / 'local' / 'remote'
 * @param $assets_path
 *   Either empty ('') if files should not be copied to the assets path, or
 *   the path to a writable directory.
 */
function xmlrpc_synch_file_get_export_data($file, $export_mode, $assets_path) {
  show_msg('xmlrpc_synch_file_get_export_data');
  $export_data = url($file->filepath, array('absolute' => TRUE));

  return $export_data;
}

/**
 * Replaces the files directory portion of a path with a substition
 * this forces clients decoding the node export to use their own
 * files directory path.  This helps get around issues with multi-site
 * installations and non-standard files directory locations.
 */
function xmlrpc_synch_file_clean_local_file_path(&$path) {
  $path = preg_replace('/^' . preg_quote(file_directory_path(), '/') . '/', xmlrpc_synch_file_FILES_DIR_SUBSTITUTION, $path);
}

/**
 * Replaces the xmlrpc_synch_file_FILES_DIR_SUBSTITUTION with the files directory
 * path, wherever found.
 */
function xmlrpc_synch_file_unclean_local_file_path(&$path) {
  $path = strtr($path, array(xmlrpc_synch_file_FILES_DIR_SUBSTITUTION => file_directory_path()));
}

/**
 * Detects remote and local file exports and imports accordingly.
 *
 * @param &$file
 *   The file, passed by reference.
 * @return TRUE or FALSE
 *   Depending on success or failure.  On success the $file object will
 *   have a valid $file->fid attribute.
 */
function xmlrpc_synch_file_import_file(&$file) {
  // Ensure the filepath is set correctly relative to this Drupal site's
  // files directory

  xmlrpc_synch_file_unclean_local_file_path($file->filepath);
  //drupal_set_message("file import file");
  // The file is already in the right location AND either the
  // xmlrpc_synch_file_path is not set or the xmlrpc_synch_file_path and filepath
  // contain the same file
  // FIXME: same filesize does NOT mean "same file".
  if (is_file($file->filepath) &&
    (
      !is_file($file->xmlrpc_synch_file_path) ||
      (
        is_file($file->xmlrpc_synch_file_path) &&
        filesize($file->filepath) == filesize($file->xmlrpc_synch_file_path) &&
        strtoupper(dechex(crc32(file_get_contents($file->uri)))) ==
        strtoupper(dechex(crc32(file_get_contents($file->xmlrpc_synch_file_path))))
      )
    )
  ) {
    //drupal_set_message("file write record");
    drupal_write_record('files', $file);
  }
  elseif (isset($file->xmlrpc_synch_file_data)) {

    //drupal_set_message("xmlrpc_synch_file_data is set");
    //drupal_set_message("file write record");

    $directory = file_create_path(dirname($file->filepath));

    if (file_check_directory($directory, TRUE)) {
      if (file_put_contents($file->filepath, base64_decode($file->xmlrpc_synch_file_data))) {
        drupal_write_record('files', $file);
      }
    }
  }
  // The file is in a local location, move it to the
  // destination then finish the save
  elseif (isset($file->xmlrpc_synch_file_path) && is_file($file->xmlrpc_synch_file_path)) {
    $directory = file_create_path(dirname($file->filepath));

    if (file_check_directory($directory, TRUE)) {
      // The $file->xmlrpc_synch_file_path is passed to reference, and modified
      // by file_copy().  Making a copy to avoid tainting the original.
      $xmlrpc_synch_file_path = $file->xmlrpc_synch_file_path;

      drupal_set_message("file copy");
      file_copy($xmlrpc_synch_file_path, $directory, FILE_EXISTS_REPLACE);

      // At this point the $file->xmlrpc_synch_file_path will contain the
      // destination of the copied file
      $file->filepath = $xmlrpc_synch_file_path;

      drupal_write_record('files', $file);
    }
  }
  // The file is in a remote location, attempt to download it
  elseif (isset($file->xmlrpc_synch_file_url)) {
    // Need time to do the download
    ini_set('max_execution_time', 900);

    $temp_path = file_directory_temp() . '/' . md5(mt_rand()) . '.txt';
    if (($source = fopen($file->xmlrpc_synch_file_url, 'r')) == FALSE) {
      show_msg(t("Could not open '@file' for reading.", array('@file' => $file->xmlrpc_synch_file_url)));
      return FALSE;
    }
    elseif (($dest = fopen($temp_path, 'w')) == FALSE) {
      show_msg(t("Could not open '@file' for writing.", array('@file' => $file->filepath)));
      return FALSE;
    }
    else {
      // PHP5 specific, downloads the file and does buffering
      // automatically.
      $bytes_read = @stream_copy_to_stream($source, $dest);

      // Flush all buffers and wipe the file statistics cache
      @fflush($source);
      @fflush($dest);
      clearstatcache();

      if ($bytes_read != filesize($temp_path)) {
        show_msg(t("Remote export '!url' could not be fully downloaded, '@file' to temporary location '!temp'.", array(
          '!url' => $file->xmlrpc_synch_file_url,
          '@file' => $file->filepath,
          '!temp' => $temp_path
        )));
        return FALSE;
      }
      // File was downloaded successfully!
      else {
        if (!@copy($temp_path, $file->filepath)) {
          unlink($temp_path);
          show_msg(t("Could not move temporary file '@temp' to '@file'.", array(
            '@temp' => $temp_path,
            '@file' => $file->filepath
          )));
          return FALSE;
        }
        show_msg($temp_path);
        unlink($temp_path);
        show_msg($temp_path);
        show_msg("<hr>");
        $file->filesize = filesize($file->filepath);
        $file->filemime = file_get_mimetype($file->filepath);
      }
    }

    fclose($source);
    fclose($dest);

    drupal_write_record('files', $file);
  }
  // Unknown error
  else {
    show_msg(t("Unknown error occurred attempting to import file: @filepath", array('@filepath' => $file->filepath)), 'error');
    return FALSE;
  }

  return TRUE;
}

/* remote get types */

function xmlrpc_synch_types_remote_get($types) {

}

function uuid_get_nid_byuuid($table, $uuid) {
  $uuid_table = 'uuid_' . $table;
  return db_result(db_query("SELECT nid FROM {uuid_node} WHERE uuid = '%s' ", $uuid));
  //SELECT nid FROM uuid_node WHERE uuid = '589f33c0-78b2-11e1-a43b-00ffb0da38f2' LIMIT 0 , 30
}

function xmlrpc_synch_views_export() {
  require_once(drupal_get_path('module', 'views') . '/includes/admin.inc');
}

function xmlrpc_synch_views_sync() {

}

function xmlrpc_synch_views_import($views_data) {
  /*
   * $views_data=array('name'=>name,
   *  'view'=>'view');
   */

  $view = '';
  views_include('view');
  // Be forgiving if someone pastes views code that starts with '<?php'.
  if (substr($views_data['view'], 0, 5) == '<?php') {
    $views_data['view'] = substr($views_data['view'], 5);
  }
  ob_start();
  eval($views_data['view']);
  ob_end_clean();

  if (!is_object($view)) {
//    return form_error($form['view'], t('Unable to interpret view code.'));
    show_msg($views_data['name'] . ' <- Unable to interpret view code.');
    return;
  }

  if (empty($view->api_version) || $view->api_version <= 2) {
    // Check for some value that would only exist on a Views 1 view.
    if (isset($view->url) || isset($view->page) || isset($view->block)) {
      views_include('convert');
      $view = views1_import($view);
      show_msg(t('You are importing a view created in Views version 1. You may need to adjust some parameters to work correctly in version 2.'), 'warning');
    }
    else {
      //  form_error($form['view'], t('That view is not compatible with this version of Views.'));
      show_msg($views_data['name'] . ' <- That view is not compatible with this version of Views..');
    }
  }
  elseif ($view->api_version > views_api_version()) {
    //form_error($form['view'], t('That view is created for the version @import_version of views, but you only have @api_version', array(
//      '@import_version' => $view->api_version,
//      '@api_version' => views_api_version())));
  }
  show_msg($views_data['name'] . ' <- ' . t('That view is created for the version @import_version of views, but you only have @api_version', array(
      '@import_version' => $view->api_version,
      '@api_version' => views_api_version()
    )));

  // View name must be alphanumeric or underscores, no other punctuation.
  if (!empty($views_data['name']) && preg_match('/[^a-zA-Z0-9_]/', $views_data['name'])) {
    //form_error($form['name'], t('View name must be alphanumeric or underscores only.'));
    show_msg(t('View name must be alphanumeric or underscores only.'));
  }

  if ($views_data['name']) {
    $view->name = $views_data['name'];
  }

  $test = views_get_view($view->name);
  if ($test && $test->type != t('Default')) {
    //form_set_error('', t('A view by that name already exists; please choose a different name'));
    show_msg(t('A view by that name already exists; please choose a different name'));
  }

  $view->init_display();

  $broken = FALSE;
  // Make sure that all plugins and handlers needed by this view actually exist.
  foreach ($view->display as $id => $display) {
    if (empty($display->handler) || !empty($display->handler->broken)) {
      show_msg(t('Display plugin @plugin is not available.', array('@plugin' => $display->display_plugin)), 'error');
      $broken = TRUE;
      continue;
    }

    $plugin = views_get_plugin('style', $display->handler->get_option('style_plugin'));
    if (!$plugin) {
      show_msg(t('Style plugin @plugin is not available.', array('@plugin' => $display->handler->get_option('style_plugin'))), 'error');
      $broken = TRUE;
    }
    else {
      if ($plugin->uses_row_plugin()) {
        $plugin = views_get_plugin('row', $display->handler->get_option('row_plugin'));
        if (!$plugin) {
          show_msg(t('Row plugin @plugin is not available.', array('@plugin' => $display->handler->get_option('row_plugin'))), 'error');
          $broken = TRUE;
        }
      }
    }

    foreach (views_object_types() as $type => $info) {
      $handlers = $display->handler->get_handlers($type);
      if ($handlers) {
        foreach ($handlers as $id => $handler) {
          if ($handler->broken()) {
            show_msg(t('@type handler @table.@field is not available.', array(
              '@type' => $info['stitle'],
              '@table' => $handler->table,
              '@field' => $handler->field,
            )), 'error');
            $broken = TRUE;
          }
        }
      }
    }
  }

  if ($broken) {
    //form_set_error('', t('Unable to import view.'));
    show_msg(t('Unable to import view.'));
  }


  views_ui_cache_set($view);
  //$form_state['redirect'] = 'admin/build/views/edit/' . $form_state['view']->name;
}

function xmlrpc_synch_syncronise_node($form) {

  $form = array();
  $last_data_check = variable_get('xmlrpc_synch_last_data_check', 0);
  xmlrpc_synch_get_data_count($last_data_check);
  drupal_set_message("Осталось обновить ноды начиная с " . $last_data_check);
  drupal_set_message("Осталось обновить ноды начиная с " . format_date($last_data_check, 'medium'));
  $count = xmlrpc_synch_get_count($last_data_check);
  $total = xmlrpc_synch_get_count(0);
  drupal_set_message("Осталось синзронизировать : " . $count . '/' . $total);
  $url = variable_get('xmlrpc_synch_server_url', 'http://localhost/studios-seo/lukukla/xmlrpc.php');
  drupal_set_message("Синхронизировать с сайта: " . $url);
  drupal_set_message(file_directory_path());
  $form['xmlrpc_synch_step'] = array(
    '#type' => 'textfield',
    '#title' => 'Шаг обновления',
    '#default_value' => 30,
    '#description' => t("If cron is enables then when site runs cron its "),
  );

  $form['xmlrpc_synch_null'] = array(
    '#type' => 'checkbox',
    '#title' => 'Обнулить дату последней синхронизации (т.е. обновить ноды с нуля)',
    '#default_value' => 0,
    '#description' => t("If cron is enables then when site runs cron its "),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#title' => 'update',
    '#value' => 'update',
  );
  $form['bulk_submit'] = array(
    '#type' => 'submit',
    '#title' => 'update',
    '#value' => 'bulk_update',
  );
  return $form;
}

function xmlrpc_synch_syncronise_node_submit($form, $form_state) {
  $step = $form_values['values']['xmlrpc_synch_step'];

  if ($form_values['values']['xmlrpc_synch_null'] == 1) {
    variable_set('xmlrpc_synch_last_data_check', 0);
  }

  if ($form_state['values']['op'] == 'update') {


    xmlrpc_synch_get_last_data_client();
  }
  else {

    $res = db_query("SELECT nid  FROM {node} where type='product' Order by nid ASC");
    while ($val = db_fetch_array($res)) {
      $nids[] = $val['nid'];
      $true = TRUE;
      $operations[] = array('xmlrpc_synch_batch_syncronize', array($true));
      $title = t('Synch nodes');
    }

    //drupal_set_message("<pre>".print_r($operations,true)."</pre>");
    $batch = array(
      'title' => $title,
      'operations' => $operations,
    );

    batch_set($batch);
    batch_process();


  }

}

function xmlrpc_synch_get_count($last_data_check = 0) {
  $url = variable_get('xmlrpc_synch_server_url', 'http://lukukla.ru/xmlrpc.php');
  $count = xmlrpc($url, 'xmlrpc_synch.get_data_count', $last_data_check, 0);
  return $count;
}

function xmlrpc_synch_get_data_count($changed = 0) {
  $count_query = db_query("SELECT count(nid) as 'count' FROM {node} where type='product' and changed>=%d", $changed);
  $val = db_fetch_array($count_query);
  return $val['count'];
}

function xmlrpc_synch_no_image_nodes_check($nid) {

  $no_images = variable_get('xmlrpc_synch_no_images', array());

  $node = node_load($nid);
  if (empty($node)) {
    drupal_set_message('node_empty');
    return FALSE;
  }

  if (file_exists($node->field_pic600x900[0]['filepath'])) {

    if (isset($no_images[$nid])) {
      unset($no_images[$nid]);
    }

    drupal_set_message('file exists');
  }
  else {
    drupal_set_message('file empty ' . $node->field_pic600x900[0]['filepath']);
    //drupal_set_message("<pre>".print_r($node->field_pic600x900,true)."</pre>");
    $problem_node = array();
    $problem_node['nid'] = $nid;
    $problem_node['title'] = $node->title;
    $no_images[$nid] = $problem_node;
  }

  $no_images = variable_set('xmlrpc_synch_no_images', $no_images);
}

function xmlrpc_synch_no_image_nodes_prepare() {
  $no_images = variable_get('xmlrpc_synch_no_images', array());
  $container = array();
  foreach ($no_images as $val) {
    //$container[]
  }
}

function xmlrpc_synch_no_image_nodes($form) {
  $form = array();

  $operations = array();
  $res = db_query("SELECT nid  FROM {node} where type='product' Order by nid ASC");
  while ($val = db_fetch_array($res)) {

    $operations[] = array(
      'xmlrpc_synch_no_image_nodes_check',
      array($val['nid'])
    );
  }


  //xmlrpc_synch_no_image_nodes_check(733);
  $no_images = variable_get('xmlrpc_synch_no_images', array());


  drupal_set_message(count($no_images));
  $header = array('title', 'link');

  $form['table'] = array(
    '#value' => theme('table', $header, $no_images),
    '#type' => 'markup',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'recheck',
  );
  $form['download'] = array(
    '#type' => 'submit',
    '#value' => 'download',
  );
  //xmlrpc_synch_images_download(682);
  $url = variable_get('xmlrpc_synch_server_url', 'http://lukukla.ru/xmlrpc.php');
  $uuid = '61161666828-c054-11e1-a345-0019bb31f432';
  $data = xmlrpc($url, 'xmlrpc_synch.get_node_by_uuid', $uuid);
  drupal_set_message("xmlrpc_synch.get_node_by_uuid $url");
  drupal_set_message(print_r($data, TRUE));
  //$uuid='61161666828-c054-11e1-a345-0019bb31f432';
  //$new_node=xmlrpc_synch_get_node_by_uuid($uuid);
//   drupal_set_message("<pre>".print_r($new_node,true)."</pre>");

  return $form;
}

function xmlrpc_synch_no_image_nodes_submit($form, $form_state) {

  //drupal_set_message(print_r($form_state['values']['op'],true));
  if ($form_state['values']['op'] == 'recheck') {
    $nids = array();
    $operations = array();
    $res = db_query("SELECT nid  FROM {node} where type='product' Order by nid ASC");
    while ($val = db_fetch_array($res)) {
      $nids[] = $val['nid'];
      $operations[] = array(
        'xmlrpc_synch_no_image_nodes_check',
        array($val['nid'])
      );
      $title = t('Resave nodes');
    }
  }
  else {
    $no_images = variable_get('xmlrpc_synch_no_images', array());
    $i = 0;
    $count = 20;
    $operations = array();
    $title = "download";
    foreach ($no_images as $val) {
      if ($i++ == $count) {
        // break;
      }
      $operations[] = array('xmlrpc_synch_images_download', array($val['nid']));
    }
  }
  //drupal_set_message("<pre>".print_r($operations,true)."</pre>");
  $batch = array(
    'title' => $title,
    'operations' => $operations,
  );

  batch_set($batch);
  batch_process();
}

function xmlrpc_synch_images_download($nid) {
  $node = node_load($nid);

  $no_images = variable_get('xmlrpc_synch_no_images', array());

  if (empty($node)) {
    //drupal_set_message("node_empty $nid" . l('link','node/'.$nid));
    return FALSE;
  }
  else {
    drupal_set_message("node exists $nid" . l('link', 'node/' . $nid));
  }

//       drupal_set_message($node->uuid);


  /*
    $file['filepath']=str_replace ('#FILES_DIRECTORY_PATH#','sites/default/files',$file['filepath']);
    $node->field_pic600x900[$key]['filepath']=str_replace ('#FILES_DIRECTORY_PATH#','sites/default/files',$file['filepath']);

    drupal_set_message($node->title.' файл '.$file['filename'].' копируем с '.$file['xmlrpc_synch_file_url'].'  в '.$file['filepath']);
    copy($file['xmlrpc_synch_file_url'],$file['filepath']);

    // file_copy($file['xmlrpc_synch_file_url'], 0, FILE_EXISTS_REPLACE);

    $file_field=field_file_save_file($file['filepath'], array(), file_directory_path(), NULL);
    drupal_set_message(print_r($file_field,true));
    $node->field_pic600x900[$key]=$file_field;
    drupal_set_message($node->title.' файл по пути.'.$node->field_pic600x900[$key]['filepath']);
   */
  //xmlrpc_synch_get_node_by_uuid($uuid)
  if ((count($node->field_pic600x900) == 0) || (count($node->field_pic600x900[0]) == 0)) {
    drupal_set_message('no images');
    return FALSE;
  }
  else {

    drupal_set_message(count($node->field_pic600x900));
    drupal_set_message('<pre>' . print_r($node->field_pic600x900, TRUE) . '</pre>');
  }

  foreach ($node->field_pic600x900 as $key => $val) {

    drupal_set_message("http://lukukla.ru/" . $val['filepath']);
    $external_link = "http://lukukla.ru/" . $val['filepath'];
    if (external_file_existst($external_link)) {
      drupal_set_message("<img src='$external_link' width='100px'/>");
      drupal_set_message("copy from $external_link to " . $val['filepath']);
      copy($external_link, $val['filepath']);
    }
    else {
      drupal_set_message(" don't exists $external_link");
      $external_link = str_replace("_0.", ".", $external_link);
      $external_link = str_replace("_1.", ".", $external_link);

      drupal_set_message("<img src='$external_link' width='100px'/>");
      drupal_set_message("copy from $external_link to " . $val['filepath']);
      if (external_file_existst($external_link)) {
        copy($external_link, $val['filepath']);
      }
      else {
        drupal_set_message(" don't exists $external_link");
      }
    }
    if (file_exists($val['filepath'])) {
      if (isset($no_images[$nid])) {

        unset($no_images[$nid]);
      }
    }
  }
  $no_images = variable_set('xmlrpc_synch_no_images', $no_images);
}

function external_file_existst($url) {
  $Headers = @get_headers($url);
// проверяем ли ответ от сервера с кодом 200 - ОК
//if(preg_match("|200|", $Headers[0])) { // - немного дольше :)
  drupal_set_message($Headers[0]);
  drupal_set_message(strpos('200', $Headers[0]));
  //if(strpos('200', $Headers[0])) {
//if($Headers[0]=="HTTP/1.1 200 OK"){
  if (preg_match("|200|", $Headers[0])) { // - немного дольше :)
    drupal_set_message('exitst');
    return TRUE;
  }
  else {
    drupal_set_message('empty');
    return FALSE;
  }
}