<?php


$plugin = array(
  'single' => TRUE,
  // Just do this one, it is needed.
  'title' => t('News title panels'),
  // Title to show up on the pane screen.
  'description' => t('Description'),
  // Description to show up on the pane screen.
  'category' => t('MinskCyclingClub'),
  // A category to put this under.
  'edit form' => 'veloclub_news_title_edit_form',
  // A function that will return the settings form for the pane.
  'render callback' => 'veloclub_news_title_render',
  // A function that will return the renderable content.
  'admin info' => 'veloclub_news_title_admin_info',
  // A function that will return the information displayed on the admin screen (optional).
  'defaults' => array( // Array of defaults for the settings form.
    'text' => '',
  ),
  'all contexts' => TRUE,
  // This is NEEDED to be able to use substitution strings in your pane.
);


/**
 * An edit form for the pane's settings.
 */
function veloclub_news_title_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Panel Text'),
    '#description' => t('Text to display, it may use substitution strings'),
    '#default_value' => $conf['text'],
  );

  return $form;
}

/**
 * Submit function, note anything in the formstate[conf] automatically gets saved
 * Notice, the magic that automatically does that for you.
 */
function veloclub_news_title_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function veloclub_news_title_render($subtype, $conf, $args, $contexts) {

  // Update the strings to allow contexts.
  if (!empty($contexts)) {
    $content = ctools_context_keyword_substitute($conf['text'], array(), $contexts);
  }

  $block = new stdClass();

  // initial content is blank
  $block->title = '';
  $block->content = $content;
  $node = node_load(arg(1));


  $block->content = '';
  $block->content .= '<div class="pane-page-wrapper">';
  if (isset($node->field_news_subribric[LANGUAGE_NONE][0]['tid'])) {
    $taxonomy = taxonomy_term_load($node->field_news_subribric[LANGUAGE_NONE][0]['tid']);
    if ($taxonomy) {
      $block->content .= '<div class="pane-page-subrubric">' . $taxonomy->name . '</div>';
    }
  }
  $block->content .= '<div class="pane-page-title"><h1>' . $node->title . '</h1></div>';
  $block->content .= '<div class="pane-page-created">' . format_date($node->created, $type = 'medium') . '</div>';


  $block->content .= '</div>';
  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function veloclub_news_title_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = $conf['text'];
    return $block;
  }
}

