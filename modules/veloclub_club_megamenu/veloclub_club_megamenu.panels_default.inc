<?php
/**
 * @file
 * veloclub_club_megamenu.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function veloclub_club_megamenu_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'news';
  $mini->category = 'megamenu';
  $mini->admin_title = 'News';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '9d6b5a17-7529-4f19-b0e6-ad66ce6c855b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f041bc7a-c93b-4c59-b916-44fb4b0d36f5';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-news-category';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f041bc7a-c93b-4c59-b916-44fb4b0d36f5';
    $display->content['new-f041bc7a-c93b-4c59-b916-44fb4b0d36f5'] = $pane;
    $display->panels['center'][0] = 'new-f041bc7a-c93b-4c59-b916-44fb4b0d36f5';
    $pane = new stdClass();
    $pane->pid = 'new-bf6af50c-6c41-4b4b-864c-b2d20cdc2541';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'taxonomy_list';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'bf6af50c-6c41-4b4b-864c-b2d20cdc2541';
    $display->content['new-bf6af50c-6c41-4b4b-864c-b2d20cdc2541'] = $pane;
    $display->panels['center'][1] = 'new-bf6af50c-6c41-4b4b-864c-b2d20cdc2541';
    $pane = new stdClass();
    $pane->pid = 'new-9fefc68a-6429-4d1d-904d-75deaa1804c0';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '1',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_3',
      'override_title' => 0,
      'override_title_text' => 'Latest news',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '9fefc68a-6429-4d1d-904d-75deaa1804c0';
    $display->content['new-9fefc68a-6429-4d1d-904d-75deaa1804c0'] = $pane;
    $display->panels['center'][2] = 'new-9fefc68a-6429-4d1d-904d-75deaa1804c0';
    $pane = new stdClass();
    $pane->pid = 'new-495e06ae-f23c-4e5b-93f5-045713ed6084';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_4',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '495e06ae-f23c-4e5b-93f5-045713ed6084';
    $display->content['new-495e06ae-f23c-4e5b-93f5-045713ed6084'] = $pane;
    $display->panels['center'][3] = 'new-495e06ae-f23c-4e5b-93f5-045713ed6084';
    $pane = new stdClass();
    $pane->pid = 'new-debe0490-00d4-4100-a3f4-e284adab6566';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'posters';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'debe0490-00d4-4100-a3f4-e284adab6566';
    $display->content['new-debe0490-00d4-4100-a3f4-e284adab6566'] = $pane;
    $display->panels['center'][4] = 'new-debe0490-00d4-4100-a3f4-e284adab6566';
    $pane = new stdClass();
    $pane->pid = 'new-62d24a71-7df0-408e-bbb7-809a82a4a207';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '7',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'megamenu-world-news',
      'css_class' => 'megamenu-world-news',
    );
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '62d24a71-7df0-408e-bbb7-809a82a4a207';
    $display->content['new-62d24a71-7df0-408e-bbb7-809a82a4a207'] = $pane;
    $display->panels['center'][5] = 'new-62d24a71-7df0-408e-bbb7-809a82a4a207';
    $pane = new stdClass();
    $pane->pid = 'new-6a7de81c-e70c-45c5-bf63-c2e47d411786';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_9',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '6a7de81c-e70c-45c5-bf63-c2e47d411786';
    $display->content['new-6a7de81c-e70c-45c5-bf63-c2e47d411786'] = $pane;
    $display->panels['center'][6] = 'new-6a7de81c-e70c-45c5-bf63-c2e47d411786';
    $pane = new stdClass();
    $pane->pid = 'new-e4a0b11d-82b9-4a95-b434-eba9215a03bb';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '1',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_10',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'e4a0b11d-82b9-4a95-b434-eba9215a03bb';
    $display->content['new-e4a0b11d-82b9-4a95-b434-eba9215a03bb'] = $pane;
    $display->panels['center'][7] = 'new-e4a0b11d-82b9-4a95-b434-eba9215a03bb';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['news'] = $mini;

  return $export;
}
