<?php
/**
 * @file
 * veloclub_club_megamenu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function veloclub_club_megamenu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}
