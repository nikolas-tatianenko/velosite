<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */


/**
 * Add unique class (mlid) to all menu items.
 * with Menu title as class
 */

/**
 * Main menu
 * Implements theme__menu_tree().
 */
function veloclub_theme_menu_tree__main_menu($variables) {
  return $variables['tree'];
}

/**
 * Main menu
 * Implements theme__menu_link().
 */
function veloclub_theme_menu_link__main_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $element['#localized_options']['html'] = TRUE;
  $output = l('<em>!! ' . $element['#title'] . '</em>', $element['#href'], $element['#localized_options']);
  return $output . $sub_menu . "\n";
}