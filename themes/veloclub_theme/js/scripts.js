(function ($) {

  Drupal.behaviors.veloclub_theme = {
    attach: function (context, settings) {
      $('.example', context).click(function () {
        $(this).next('ul').toggle('show');
      });

      $('img', context).click(function () {
        $(this).next('ul').toggle('show');
      });

      // Open close logic.
      $('.views-field-field-date .open-results', context).click(function () {
        //console.log('field-field-date');
        if ($(this).hasClass('has-results') && !$(this).parent().parent().parent().hasClass('disabled')) {
          $('div.view.view-races.view-id-races.view-display-id-block_3 div.view-content ul li, div.view.view-races.view-id-races.view-display-id-block_5 div.view-content ul li').not($(this).parent().parent().parent()).toggleClass('disabled');
          if ($(this).hasClass('closed')) {
            $(this).removeClass('closed').addClass('open');
            $(this).find('.trigger').text('‒');
            console.log($(this).parent().parent().parent().attr('class'));
            $(this).parent().parent().parent().find('div.views-field.views-field-field-results-collection').css('display', 'inline-block');
          } else {
            $(this).removeClass('open').addClass('closed');
            $(this).find('.trigger').text('+');
            $(this).parent().parent().parent().find('div.views-field.views-field-field-results-collection').css('display', 'none');
          }
        }
      });

      function veloclub_split_str_by_lines(str, symbols_per_line) {
        var arr, linelength = 0, i = 0, line = '', lines = [], j = 0, output;
        arr = str.split(' ');
        for (var i = 0; i < arr.length; i++) {
          if (typeof arr[i] != 'undefined') {
            if (linelength >= 0 && (linelength + arr[i].length) < symbols_per_line) {
              line = line + ' ' + arr[i];
              linelength = linelength + arr[i].length;

            } else {
              lines[j] = line;
              j++;
              line = arr[i];
              linelength = arr[i].length;
            }
            if (i == arr.length - 1) {
              lines[j] = line;
            }
          }
        }
        i = 0;
        output = '<div class="title">';
        for (i = 0; i < lines.length; i++) {
          output = output + '<p>' + lines[i] + '</p>';
        }
        output = output + '</div>';
        return output;
      }

      // Change last name size for cyclists.

	  var resize_fullname = function (coof) {
		var font_size = coof * 60;
		jQuery('.node-type-cyclist #content-type-panel div.field-name-field-last-name .field-item, .node-type-trainer #content-type-panel div.field-name-field-last-name .field-item').css('font-size', font_size + 'px');
	  }
	  if (jQuery('body').hasClass('node-type-cyclist')) {
        var coof, font_size;
        coof = jQuery('.node-type-cyclist #content-type-panel div.panel-pane.pane-entity-field.pane-node-field-last-name, .node-type-trainer #content-type-panel div.panel-pane.pane-entity-field.pane-node-field-last-name').width() / jQuery('.node-type-cyclist #content-type-panel div.field-name-field-last-name .field-item, .node-type-trainer #content-type-panel div.field-name-field-last-name .field-item').width();
		
		console.log(coof);
        if (coof <= 1) {
			resize_fullname(coof);
        } else {
			coof = 10/jQuery('.node-type-cyclist #content-type-panel div.field-name-field-last-name .field-item, .node-type-trainer #content-type-panel 	div.field-name-field-last-name .field-item').html().length;
			if(coof <=1 ) {
				resize_fullname(coof);
			}
		}
      }
	  
	  // Splitting titles by lines.
      $('.view-slideshow .connected-carousels .stage .carousel li .views-field-title a,.node-type-club-pages #content-type-panel div.panel-panel div.panel-pane.pane-page-title h1,div#commands-landing-page div.view.view-team-frontpage div.views-field.views-field-title a, .club-landing-page-wrapper #landing-page .pane-node .pane-title').each(function (index) {
        $(this).html(veloclub_split_str_by_lines($(this).text(), 25));
      });
      $('div.view.view-photo.view-id-photo.view-display-id-block_1 div.views-field.views-field-title a').each(function (index) {
        $(this).html(veloclub_split_str_by_lines($(this).text(), 10));
      });

      $('.big-news-block-wrapper div.views-field.views-field-title a, div.view.view-photo.view-display-id-big_photo_block .views-row div.views-field.views-field-title a').each(function (index) {
        $(this).html(veloclub_split_str_by_lines($(this).text(), 25));
      });
      // Set active menu items.
      if (typeof Drupal.settings.node.news_category_menu_alias != 'undefined') {
        $('.pane-menu-menu-news-category ul.menu li a[href="' + Drupal.settings.node.news_category_menu_alias + '"]').parent().addClass('active-trail');
      }
      if (typeof Drupal.settings.node.main_menu_alias != 'undefined') {
        $('ul#main-menu li a[href="' + Drupal.settings.node.main_menu_alias + '"]').parent().addClass('active-trail');
      }
      if (typeof Drupal.settings.node.main_menu_alias != 'undefined') {
        $('ul#main-menu li a[href="' + Drupal.settings.node.main_menu_alias + '"]').parent().addClass('active-trail');
      }
      if (typeof Drupal.settings.node.club_menu_alias != 'undefined') {
        $('.pane-menu-menu-club-menu ul.menu li a[href="http://minskcyclingclub.by' + Drupal.settings.node.club_menu_alias + '"]').parent().addClass('active-trail');
      }
      (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=748941311796178&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));


      // This is the connector function.
      // It connects one item from the navigation carousel to one item from the
      // stage carousel.
      // The default behaviour is, to connect items with the same index from both
      // carousels. This might _not_ work with circular carousels!
      var connector = function (itemNavigation, carouselStage) {
        return carouselStage.jcarousel('items').eq(itemNavigation.index());
      };
      /*$(window).load(function () {
       $('.view-partners img').hoverizr();

       });*/
      $(function () {
        // Setup the carousels. Adjust the options for both carousels here.
        var carouselStage = $('.carousel-stage').jcarousel();
        var carouselNavigation = $('.carousel-navigation').jcarousel(
          {
            scroll: 4,
            animation: 500,
            auto: 0,
            wrap: "both",
            itemFallbackDimension: 400,
            vertical: true,
            perPage: 4
          });
        //	alert('test');
        /*
         $('.view-news.view-display-id-block_4 ul').jcarousel({perPage: 3,vertical:true,scroll: 1,
         auto: 2});*/


        $(function () {
          //$('.view-news.view-display-id-block_4 ul').jcarousel();


          /*
           $('.view-news.view-display-id-block_4 .jcarousel-pagination')
           .on('jcarouselpagination:active', 'a', function() {
           $(this).addClass('active');
           })
           .on('jcarouselpagination:inactive', 'a', function() {
           $(this).removeClass('active');
           })
           .jcarouselPagination();
           */
          $('.view-news.view-display-id-block_4 .jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
              $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
              $(this).removeClass('active');
            })
            .on('click', function (e) {
              e.preventDefault();
            })
            .jcarouselPagination({
              perpage: 4,
              item: function (page) {
                return '<a href="#' + page + '">' + page + '</a>';
              }
            });

        });


        // We loop through the items of the navigation carousel and set it up
        // as a control for an item from the stage carousel.
        carouselNavigation.jcarousel('items').each(function () {
          var item = $(this);

          // This is where we actually connect to items.
          var target = connector(item, carouselStage);

          item
            .on('jcarouselcontrol:active', function () {
              carouselNavigation.jcarousel('scrollIntoView', this);
              item.addClass('active');
            })
            .on('jcarouselcontrol:inactive', function () {
              item.removeClass('active');
            })
            .jcarouselControl({
              target: target,
              carousel: carouselStage
            });
        });

        // Setup controls for the stage carousel
        $('.prev-stage')
          .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
          })
          .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
          })
          .jcarouselControl({
            target: '-=1'
          });

        $('.next-stage')
          .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
          })
          .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
          })
          .jcarouselControl({
            target: '+=1'
          });

        // Setup controls for the navigation carousel
        $('.prev-navigation')
          .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
          })
          .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
          })
          .jcarouselControl({
            target: '-=4'
          });

        $('.next-navigation')
          .on('jcarouselcontrol:inactive', function () {
            $(this).addClass('inactive');
          })
          .on('jcarouselcontrol:active', function () {
            $(this).removeClass('inactive');
          })
          .jcarouselControl({
            target: '+=4'
          });


        $('.view-slideshow .jcarousel-pagination')
          .on('jcarouselpagination:active', 'a', function () {
            $(this).addClass('active');
          })
          .on('jcarouselpagination:inactive', 'a', function () {
            $(this).removeClass('active');
          })
          .on('click', function (e) {
            e.preventDefault();
          })
          .jcarouselPagination({
            perpage: 4,
            item: function (page) {
              return '<a href="#' + page + '">' + page + '</a>';
            }
          });


      });

    }
  };
})(jQuery);