<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * @ingroup views_templates
 */
print $fields['field_image']->content;
?>
<div class='slide-text-content'>
  <?php
  print $fields['title']->wrapper_prefix;
  print $fields['title']->content;
  print $fields['title']->wrapper_suffix;
  print $fields['view_node']->wrapper_prefix;
  print $fields['view_node']->content;
  print $fields['view_node']->wrapper_suffix;
  ?>
</div>