<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php //print $head_title; ?></title>
  <?php //print $head; ?>
  <?php //print $styles; ?>
  <?php //print $scripts; ?>
  <title>Minsk Cycling Club</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="margin: 0px auto; text-align: center; background-color: #181C4B; background-position: initial initial; background-repeat: initial initial;">
<link rel="shortcut icon" href="/drim.ico">
<img src="sites/velosport/files/maintainance/zastavka6_2.jpg" alt="minskcyclingclub" style="width: 60%;height:auto;margin: 0px auto;">

</body></html>