<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="wrapper">
  <div class="connected-carousels">
    <div class="stage">
      <div class="carousel carousel-stage">
        <ul>
          <?php foreach ($rows as $id => $row): ?>
            <li class="<?php print $classes_array[$id]; ?>">
				<?php print $row; ?>
			</li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>

    <div class="navigation">
      <a href="#" class="prev prev-navigation">&lsaquo;</a>
      <a href="#" class="next next-navigation">&rsaquo;</a>

      <div class="carousel carousel-navigation">
        <ul>
          <?php foreach ($rows as $id => $row): ?>
            <li class="<?php print $classes_array[$id]; ?>">
				<?php print $row; ?>
			</li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class='jcarousel-pagination-wrapper'>
		<p class="jcarousel-pagination"></p>
		</div>
    </div>
  </div>
</div>
