<?php
/**
 * Implements hook_form_system_theme_settings_alter()
 */
function veloclub_theme_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['left_bg'] = array(
    '#type' => 'file',
    '#title' => t('Left BG'),
    '#default_value'  => theme_get_setting('left_bg'),
    '#description' => t('Left Bg'),
  );

  $form['right_bg'] = array(
    '#type' => 'file',
    '#title' => t('Right BG'),
    '#default_value'  => theme_get_setting('right_bg'),
    '#description' => t('Right BG'),
  );
  dsm($form);
  //$form['#submit']
}